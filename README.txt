CamelForth Documentation
========================

Welcome to the CamelForth project for Z80-based targets including:
* Cambridge Z88
* Sinclair ZX Spectrum +3/+3e
* PetersPlus Sprinter

This version of CamelForth is fully compliant with the ANS Forth Standard,
and includes many of the optional wordsets.

This software is distributed under the GNU General Public License, version 3.


=== Building ===
To build CamelForth, you need the following tools:
* git, to clone the source repository from bitbucket
* gForth (other ANS Forth implementations should also be acceptable)
* z88dk (which includes the Interlogic z80asm assembler)

Optionally:
* grep, cut and sort (for glossary auto-generation)
* John Elliott's "taptools" (to generate loadable Spectrum tape/disk files, as well as raw binary)

If you use Ubuntu Linux or a variant, all these tools can be installed directly
from your package manager. Users of other systems may need to download and install
the tools from the appropriate websites.

Building is simply a matter of changing to the appropriate target directory and running
the make.sh script.


== Sources Layout & Conventions ==

The layout of the sources is straightforward. There is a **generic** directory
plus a target-specific directory for each of the existing targets:

* speccy, Sinclair ZX Spectrum +3/+3e
* sprinter, PetersPlus Sprinter
* z88, Cambridge Z88

Source files are named according to the following conventions:

* .hi, High-level (Forth) definitions
* .low, Low-level (assembly) definitions
* .dep, Target-dependent definitions (may mix Forth and assembly)
* .zx, Spectrum-specific definitions
* .spr, Sprinter-specific definitions
* .z88, Z88-specific definitions
* .frg, code fragments
* .asm, z80asm assembly files
* .def, z80asm definition files
* .fth, Forth files
* .sh, Shell scripts

For proper automatic glossary generation, each word should be documented
within the sources on a single line, laid out as follows:

* Columns 1-2: semicolon, definition class, space:
*  ;C definitions from main wordsets (eg core, file, search-order)
*  ;X definitions from extension wordsets (eg core extension, file extension, search-order extension)
*  ;Z definitions not present in the ANS standard wordsets
*  ;- headerless words
* Columns 4-19: word name
* Columns 21-47: stack picture
* Columns 49 onwards: description

All other comment lines within the sources should contain a space directly
after the semi-colon, to avoid them being erroneously selected for inclusion
in the glossary.


=== Porting to other targets ===
To port CamelForth to an alternative Z80-based system, create a new target directory
and copy in the options.asm file from one of the other targets and edit to taste.
You must also copy in any .dep & .frg files that are pulled in (according to the options
selected) and modify these appropriately for your target. Helpful comments can be found
in the source files for the existing targets.

Let me know if you wish to implement another port, and it can be hosted here on bitbucket.


=== Further documentation ===
GENERIC
  Contains end-user documentation suitable for all systems, including
  the all-important ANS document. This includes documentation for specific
  optional wordsets and loadable support files. Some files contain system-
  dependent information (indicated with notes by <<SYSDEP>> markers) and
  will need to be edited for your own system.

Z88, SPRINTER, SPECCY
  System-dependent end-user documentation, including modified generic
  documents (but not generic documents that don't need modification).


----
Garry Lancaster, 17th November 2011

