; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: Text-windowing (Sprinter-specific)
; ======================================================================


;Z SETMODE          x --                        Set video mode ($02/$03/$81/$82 + $0100 if screen 1)
    head SETMODE,7,"SETMODE",docode,0,0
        push    iy
        push    ix
        push    de
        ld      a,c
if SPRINTER_GRAPHICS
        ld      (usr_screenmode),a              ; save for testing graphics mode
endif ; SPRINTER_GRAPHICS
        ld      c,DSS_SETVMOD
        rst     DSS
        pop     de
        pop     ix
        pop     iy
        pop     bc
        next

;Z GETMODE          -- x                        Get video mode ($02/$03/$81/$82 + $0100 if screen 1)
    head GETMODE,7,"GETMODE",docode,0,0
        push    bc
        push    iy
        push    ix
        push    de
        ld      c,DSS_GETVMOD
        rst     DSS
        ld      c,a
        pop     de
        pop     ix
        pop     iy
        next

;Z SCREEN           x --                        Set active screen (0/1)
    head SCREEN,6,"SCREEN",docode,0,0
        push    iy
        push    ix
        push    de
        ld      b,c
        ld      c,DSS_SELPAGE
        rst     DSS
        pop     de
        pop     ix
        pop     iy
        pop     bc
        next

;Z WINDOW           id --                       Set active window
    head WINDOW,6,"WINDOW",docode,0,0
        ld      a,c
        and     7
        push    af              ; save id
        add     a,a
        add     a,a
        add     a,a
        add     a,a
        ld      c,a
        ld      b,0             ; BC=16*id
        ld      hl,usr_windows
        add     hl,bc
        ld      (iy+usr_activewin),l
        ld      (iy+usr_activewin+1),h
        pop     af
        ld      (hl),a          ; store id in window def
        pop     bc
        next

;Z WINDOW?          -- w h id                   Get active window details
    head WINDOWQ,7,"WINDOW?",docode,0,0
        push    bc
        push    iy
        exx
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        push    hl
        pop     iy
        ld      c,(iy+sp_originx)
        ld      b,(iy+sp_originx+1)
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)
        inc     hl
        and     a
        sbc     hl,bc
        ex      de,hl           ; DE'=w
        ld      c,(iy+sp_originy)
        ld      b,0
        ld      l,(iy+sp_limity)
        ld      h,b
        inc     hl
        and     a
        sbc     hl,bc           ; HL'=h
        ld      a,(iy+sp_number)        ; A=id
        pop     iy              ; restore IY
        push    de              ; stack w & h
        push    hl
        exx
        ld      c,a
        ld      b,0             ; BC=id
        next

;Z ATTRIB           x --                        Set attribute for current window
    head ATTRIB,6,"ATTRIB",docode,0,0
        push    bc
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        ld      bc,sp_attr
        add     hl,bc
        pop     bc
        ld      (hl),c          ; set attribs in window definition
        inc     hl
        ld      (hl),b
.attr3
if SPRINTER_GRAPHICS
        ld      a,(usr_screenmode)
        and     $80
        jr      z,attr2         ; no further action if text
        call    dosubsys
        jp      grph_gattrib    ; otherwise set graphic attrib in offscreen
endif ; SPRINTER_GRAPHICS
.attr2  pop     bc              ; get new TOS
        next
                
;Z DEFWINDOW        x y w h --                  Define current window without clearing it
    head DEFWINDOW,9,"DEFWINDOW",docode,0,0
        ld      a,c
        exx
        ld      d,a             ; D=h
        pop     bc              ; BC=w
        pop     hl
        ld      e,l             ; E=y
        pop     hl              ; HL=x
        push    iy              ; save IY
        push    de
        ld      e,(iy+usr_activewin)
        ld      d,(iy+usr_activewin+1)
        push    de
        pop     iy              ; IY=window address
        pop     de
        ld      (iy+sp_originx),l
        ld      (iy+sp_originx+1),h
        ld      (iy+sp_originy),e
        ld      (iy+sp_cursorx),l
        ld      (iy+sp_cursorx+1),h
        ld      (iy+sp_cursory),e
        add     hl,bc
        dec     hl
        ld      (iy+sp_limitx),l
        ld      (iy+sp_limitx+1),h
        ld      a,d
        add     a,e
        dec     a
        ld      (iy+sp_limity),a
if SPRINTER_GRAPHICS
        ld      (iy+sp_font),MAXFONT    ; set font 0 (done as MAXFONT-font#)
        ld      hl,vidram_fonts+64*MAXFONT
        ld      (iy+sp_fontaddr),l      ; set address for font 0
        ld      (iy+sp_fontaddr+1),h
        ld      a,(usr_screenmode)
        and     $80
        ld      (iy+sp_mode),a  ; save mode - just text/graphics bit 7
endif ; SPRINTER_GRAPHICS
        exx
        ld      c,(iy+sp_attr)
        ld      b,(iy+sp_attr+1)
        pop     iy              ; restore IY
        jr      attr3           ; set current attribs (for graphics)

;Z OPENWINDOW       x y w h --                  Define current window and clears it
    head OPENWINDOW,10,"OPENWINDOW",docolon,0,0
        defw    DEFWINDOW,PAGE,EXIT

;Z TITLEBAR         x --                        Display title bar "x" wide
;   ?DUP IF  FOR  196 EMIT  STEP  THEN ;
   nohead TITLEBAR,docolon
        defw    QDUP,QBRANCH,tbar2
        defw    TOR
.tbar1  defw    LIT,196,EMIT,BRSTEP,tbar1
.tbar2  defw    EXIT

;Z OPENTITLED       x y w h c u --              Define current window with title bar
;   2>R 2OVER 2OVER OPENWINDOW          open window and clear it
;   OVER 2R> ROT OVER - 2 -             number of '-'s to output in total
;   218 EMIT DUP 2/ TITLEBAR >R         output left-hand bar
;   TYPE                                output title
;   R> 1+ 2/ TITLEBAR 191 EMIT          output right-hand bar
;   ROT 1+ ROT ROT 1- OPENWINDOW ;      re-open window excluding top line
    head OPENTITLED,10,"OPENTITLED",docolon,0,0
        defw    TWOTOR,TWOOVER,TWOOVER,OPENWINDOW
        defw    OVER,TWORFROM,ROT,OVER,MINUS,LIT,2,MINUS
        defw    LIT,218,EMIT,DUP,TWOSLASH,TITLEBAR,TOR
        defw    TYPE
        defw    RFROM,ONEPLUS,TWOSLASH,TITLEBAR,LIT,191,EMIT
        defw    ROT,ONEPLUS,ROT,ROT,ONEMINUS,OPENWINDOW
        defw    EXIT

;Z OPENPOPUP        x y w h c u --              Define boxed window with title bar
;   2>R 2OVER 2OVER 2R> OPENTITLED      open titled window
;   DUP 2 - 1 MAX                       number of middle lines
;   FOR  179 EMIT OVER 2 - SPACES 179 EMIT  STEP
;                                       output middle lines
;   192 EMIT OVER 2 - TITLEBAR 217 EMIT output bottom line
;   2SWAP 1+ SWAP 1+ SWAP 2SWAP         redefine window origin
;   2 - SWAP 2 - SWAP                   and size
;   OPENWINDOW ;                        finally reopen
    head OPENPOPUP,9,"OPENPOPUP",docolon,0,0
        defw    TWOTOR,TWOOVER,TWOOVER,TWORFROM,OPENTITLED
        defw    DUP,LIT,2,MINUS,ONE,MAX
        defw    TOR
.popup1 defw    LIT,179,EMIT,OVER,LIT,2,MINUS,SPACES,LIT,179,EMIT,BRSTEP,popup1
        defw    LIT,192,EMIT,OVER,LIT,2,MINUS,TITLEBAR,LIT,217,EMIT
        defw    TWOSWAP,ONEPLUS,SWOP,ONEPLUS,SWOP,TWOSWAP
        defw    LIT,2,MINUS,SWOP,LIT,2,MINUS,SWOP
        defw    OPENWINDOW,EXIT


; ======================================================================
; The window I/O routines
; ======================================================================

; The terminal character output routine, called by the charout.frg fragment
; We support the following control characters:
;   8 cursor left (wrappable)
;   9 cursor right (wrappable)
;  12 clear window
;  13 carriage return

.sprinter_charout
        push    iy
        push    ix
        push    hl
        push    de
        push    bc
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        push    hl
        pop     iy                      ; IY=window address
        ld      c,a                     ; save char
if SPRINTER_GRAPHICS
        bit     7,(iy+sp_mode)
        jr      z,charout_textmode      ; move on if a text-mode window
        di                              ; interrupts must be disabled throughout
        in      a,(seg0)
        ld      (data_seg0page),a       ; save segment 0 binding
        ld      a,(data_graphpage)
        out     (seg0),a                ; bind to graphics subsystem
        call    grph_charout            ; output the graphics character
        ld      a,(data_seg0page)
        out     (seg0),a                ; rebind segment 0
        ei
        jr      endcharout
endif ; SPRINTER_GRAPHICS
.charout_textmode
        ld      d,(iy+sp_cursory)
        ld      e,(iy+sp_cursorx)
        call    validate_pos    ; DE=validated position
        ld      a,c             ; A=char
        cp      out_char_left
        jr      z,do_char_left
        cp      out_char_right
        jr      z,do_char_right
        cp      12
        jr      z,do_char_clear
        cp      13
        jr      z,do_char_newline
        push    de
        ld      b,(iy+sp_attr)  ; B=attribute
        push    iy
        ld      c,DSS_WRCHAR
        rst     DSS             ; output character
        pop     iy
        pop     de
.do_char_right
        inc     e               ; update position
.move_cursor
        ld      (iy+sp_cursory),d
        ld      (iy+sp_cursorx),e
.endcharout
        pop     bc
        pop     de
        pop     hl
        pop     ix
        pop     iy
        ret
.do_char_left
        ld      a,e
        cp      (iy+sp_originx)
        jr      z,left_wrap
        dec     e               ; update position
        jr      move_cursor
.left_wrap
        ld      a,d
        cp      (iy+sp_originy)
        jr      z,move_cursor   ; do nothing if at top left
        ld      a,(iy+sp_limitx)
        ld      e,a             ; E=rightmost column
        dec     d               ; up one line
        jr      move_cursor
.do_char_newline
        ld      e,(iy+sp_originx) ; start of line
        inc     d               ; next line
        jr      move_cursor
.do_char_clear
        ld      d,(iy+sp_originy)
        ld      e,(iy+sp_originx)
        ld      (iy+sp_cursory),d       ; reset the cursor position
        ld      (iy+sp_cursorx),e
        ld      h,(iy+sp_limity)
        inc     h
        ld      l,(iy+sp_limitx)
        inc     l
        and     a
        sbc     hl,de           ; H=height, L=width
        ld      b,(iy+sp_attr)  ; B=attribute
        ld      a,' '
                                ; don't bother to save IY as we're done
        ld      c,DSS_CLEAR
        rst     DSS             ; clear the window
        jr      endcharout

.validate_pos
        ld      a,(iy+sp_limitx)
        cp      e
        jr      nc,columnokay
        ld      e,(iy+sp_originx)
        inc     d
.columnokay
        ld      a,(iy+sp_limity)
        cp      d
        jr      nc,rowokay
        dec     d
        push    bc
        push    de
        ld      d,(iy+sp_originy)
        ld      e,(iy+sp_originx)
        ld      h,(iy+sp_limity)
        inc     h
        ld      l,(iy+sp_limitx)
        inc     l
        and     a
        sbc     hl,de           ; H=height, L=width
        push    de
        push    hl
        ld      b,1
        ld      a,b             ; bottom row clearing doesn't work as expected
        push    iy
        ld      c,DSS_SCROLL
        rst     DSS
        pop     iy
        pop     hl
        pop     de
        ld      d,(iy+sp_limity)
        ld      h,1             ; 1 line to clear at bottom
        ld      b,(iy+sp_attr)  ; B=attribute
        ld      a,' '
        push    iy
        ld      c,DSS_CLEAR
        rst     DSS             ; clear the line
        pop     iy
        pop     de
        pop     bc
.rowokay
        ld      (iy+sp_cursory),d
        ld      (iy+sp_cursorx),e
        ret


; Cursor hide/show

.sprinter_cursor
        push    iy
        push    ix
        push    hl
        push    de
        push    bc
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        push    hl
        pop     iy
        bit     7,(iy+sp_mode)
        jr      nz,nocursor             ; graphics mode cursor not yet implemented
        ld      d,(iy+sp_cursory)
        ld      e,(iy+sp_cursorx)
        call    validate_pos
        push    iy
        ld      c,DSS_RDCHAR
        rst     DSS             ; read the existing character
        pop     iy
        ld      e,a
        ld      a,b
        and     7               ; A=existing ink colour, low 3 bits
        rlca
        rlca
        rlca
        rlca
        ld      d,a             ; D=new paper colour
        ld      a,b
        rrca
        rrca
        rrca
        rrca
        and     7               ; A=existing paper colour
        ld      c,a             ; C=new ink colour
        ld      a,b
        and     @10001000       ; A=blink/bright
        or      d               ; combine paper
        or      c               ; combine ink
        ld      b,a             ; B=attributes
        ld      a,e             ; A=char
        ld      d,(iy+sp_cursory)
        ld      e,(iy+sp_cursorx)
                                ; not bothering to save IY as we're finished now
        ld      c,DSS_WRCHAR
        rst     DSS             ; rewrite character
.nocursor
        pop     bc
        pop     de
        pop     hl
        pop     ix
        pop     iy
        ret
