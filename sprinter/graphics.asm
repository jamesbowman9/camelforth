; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: Graphics & Accelerator functions (Sprinter-specific)
; ======================================================================

        module  graphics
        org     0

        include "graphics.def"
        include "windows.def"
        include "sprinter.def"

defvars $c100 {
        uinitadd        ds.w    1               ; where UINIT is
        initsp          ds.w    1
        userst          ds.b    1               ; start of uservars
}

        include "uservars.low"

        defc    DONE=0

; ======================================================================
; Jump table
; ======================================================================


        jp      0                               ; exit subsystem (patched)
        jp      grph_gfill_entry
        jp      grph_gblt_entry
        jp      grph_gput_entry
        jp      grph_gget_entry
        jp      grph_gflood_entry
        jp      grph_setfont_entry
        jp      grph_setpattern_entry
        jp      grph_gpixel_entry
        jp      grph_gpixelq_entry
        jp      grph_gline_entry
        jp      grph_gattrib_entry
        jp      grph_charout_entry
        jp      grph_font_entry
        jp      grph_setpal_entry
        jp      grph_getpal_entry
        jp      grph_gmove_entry
        jp      grph_gposq_entry
        jp      grph_width_entry


; ======================================================================
; The graphics calls themselves
; ======================================================================

; All graphics calls ensure that YPORT is set to a safe value whenever
; memory in segment 1 might be written to (and when they exit), otherwise
; a Sprinter bug means that the screen could also be written to. 
; CamelForth's stack is always in segment 2, so stack access doesn't matter.


;- GFILL            x y w h a --                Fill graphics rectangle (assumes 320x256)
.grph_gfill_entry
        in      a,(seg1)
        ld      h,a     ; H=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        ld      a,c     ; A=attr
        exx
        pop     de      ; E=height
        ld      d,a     ; D=attr
        ld      a,e
        ld      (gfill2+1),a    ; store height for accelerator initialisation
        pop     bc      ; BC=width
        pop     hl
        ld      e,l     ; E=y
        ld      d,d     ; enable accelerator
.gfill2 ld      a,0     ; acceleration height (patched earlier)
        ld      b,b     ; switch off accelerator
        ld      a,(iy+usr_gmode)
        bit     4,a
        jr      nz,gfillpat     ; move on for pattern fills
        and     3               ; get ROP mode
        jr      nz,gfillrop     ; move on if not simple copy case

        ; This is the simple copy case, and uses the accelerator screen fill mode

        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing
.gfill3 ld      a,e
        out     (yport),a       ; set Y position
        ld      e,e     ; set accelerator fill vertical line mode
        ld      (hl),d  ; fill line with colour
        ld      b,b     ; switch off accelerator
        inc     hl      ; move to next line
        dec     bc
        ld      a,b
        or      c
        jr      nz,gfill3
.gfillend
        exx
        ld      a,h
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE

        ; This is the ROP case, and uses the accelerator screen copy mode

.gfillrop
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gfill4
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gfill4
        ld      a,$a6           ; AND (HL)
.gfill4 ld      (gfillop),a     ; store logic op
        ld      a,e
        ex      af,af'          ; A'=Y position
        ld      a,d             ; A=attrib
        ld      de,fillropbuf
        ld      c,c             ; set accelerator fill memory mode
        ld      (de),a          ; fill buffer with colour
        ld      b,b             ; switch off accelerator
        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing
.gfill5 ld      l,l             ; set accelerator memory copy mode
        ld      a,(de)          ; copy filled buffer to accelerator
        ld      b,b             ; switch off accelerator
        ex      af,af'
        out     (yport),a       ; set Y position
        ex      af,af'
        ld      a,a             ; set accelerator screen blt mode
.gfillop
        nop                     ; replaced with AND/OR/XOR (HL)
        ld      b,b             ; switch off accelerator
        ex      af,af'
        out     (yport),a       ; reset Y position
        ex      af,af'
        ld      a,a             ; set accelerator screen blt mode
        ld      (hl),a          ; store ROPped data on screen
        ld      b,b     ; switch off accelerator
        inc     hl      ; move to next line
        dec     bc
        ld      a,b
        or      c
        jr      nz,gfill5
        jr      gfillend

        ; This is the pattern fill case, with optional ROP
        ; We don't currently have a special copy case

.gfillpat
        and     3
        jr      z,gfilp1
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gfilp1
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gfilp1
        ld      a,$a6           ; AND (HL)
.gfilp1 ld      (gfilpop),a     ; store logic op
        ld      a,e
        ex      af,af'          ; A'=Y position
        ld      a,d             ; A=pattern
        and     3
        add     a,a
        add     a,a
        add     a,a
        add     a,vidram_pattern0~$ff
        ld      d,vidram_pattern0/$100
        ld      e,a             ; DE=pattern source, column 0
        pop     hl              ; HL=x
        res     7,h
        set     6,h             ; ensure seg 1 addressing
.gfilp2 push    de              ; save pattern source
        ld      a,l
        and     7
        add     a,e
        ld      e,a             ; DE=pattern source, aligned to dest column
        ex      af,af'
        out     (yport),a       ; set Y position
        ex      af,af'
        ld      a,a             ; set accelerator screen blt mode
        ld      a,(de)          ; get line of pattern
        ld      b,b             ; switch off accelerator
        ex      af,af'
        out     (yport),a       ; set Y position
        ex      af,af'
        ld      a,a             ; set accelerator screen blt mode
.gfilpop
        nop                     ; replaced with NOP or AND/OR/XOR (HL)
        ld      b,b             ; switch off accelerator
        ex      af,af'
        out     (yport),a       ; reset Y position
        ex      af,af'
        ld      a,a             ; set accelerator screen blt mode
        ld      (hl),a          ; store ROPped data on screen
        ld      b,b             ; switch off accelerator
        inc     hl              ; move to next line
        pop     de              ; restore pattern source
        dec     bc
        ld      a,b
        or      c
        jr      nz,gfilp2
        jp      gfillend


;- GBLT             x y w h x2 y2 --            Blt rectangle (assumes 320x256)
.grph_gblt_entry
        ld      l,c     ; L=y2
        in      a,(seg1)
        ld      b,a     ; B=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        ld      a,l     ; A=y2
        pop     hl      ; HL=x2
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x2
        exx
        pop     de      ; E=height
        ld      d,a     ; D=y2
        ld      a,e
        ld      (gblt2+1),a    ; store height for accelerator initialisation
        ld      d,d     ; enable accelerator
.gblt2  ld      a,0     ; acceleration height (patched earlier)
        ld      b,b     ; switch off accelerator
        pop     bc      ; BC=width
        pop     hl
        ld      e,l     ; E=y
        ld      a,(iy+usr_gmode)
        and     3               ; get ROP mode
        jr      nz,gbltrop      ; move on if not simple copy case
        
        ; This is the simple copy case

        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x

        ; So, at this point we have:
        ;  HL=x, BC'=x2, E=y, D=y2, BC=width, (gblt2+1)=height

.gblt3  ld      a,e
        out     (yport),a       ; set source Y position
        ld      a,a     ; select accelerator screen blt mode
        ld      a,(hl)  ; get vertical line from source
        ld      b,b     ; switch off accelerator
        inc     hl
        ld      a,d
        out     (yport),a       ; set dest Y position
        exx
        ld      a,a     ; select accelerator screen blt mode
        ld      (hl),a  ; store vertical line at destination
        ld      b,b     ; switch off accelerator
        inc     hl
        exx
        dec     bc
        ld      a,b
        or      c
        jr      nz,gblt3
.gbltend
        exx
        ld      a,b
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE

        ; This is the ROP case

.gbltrop
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gblt4
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gblt4
        ld      a,$a6           ; AND (HL)
.gblt4  ld      (gbltop),a      ; store logic op
        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x

        ; So, at this point we have:
        ;  HL=x, BC'=x2, E=y, D=y2, BC=width, (gblt2+1)=height

.gblt5  ld      a,e
        out     (yport),a       ; set source Y position
        ld      a,a     ; select accelerator screen blt mode
        ld      a,(hl)  ; get vertical line from source
        ld      b,b     ; switch off accelerator
        inc     hl
        ld      a,d
        out     (yport),a       ; set dest Y position
        exx
        ld      a,a     ; select accelerator screen blt mode
.gbltop nop             ; replaced with AND/OR/XOR (HL)
        ld      b,b     ; switch off accelerator
        exx
        ld      a,d
        out     (yport),a       ; reset dest Y position
        exx
        ld      a,a     ; select accelerator screen blt mode
        ld      (hl),a  ; store vertical line at destination
        ld      b,b     ; switch off accelerator
        inc     hl
        exx
        dec     bc
        ld      a,b
        or      c
        jr      nz,gblt5
        jr      gbltend


;- GPUT             x y w h addr --             Download rectangle (assumes 320x256)
.grph_gput_entry
        in      a,(seg1)
        ld      h,a     ; H=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        exx
        pop     de      ; E=height
        ld      a,e
        ld      (gput2+1),a    ; store height for accelerator initialisation
        ld      (gput4+1),a    ; and for source delta in non-ROP case
        ld      (gput9+1),a     ; and for source delta in ROP case
        ld      d,d     ; enable accelerator
.gput2  ld      a,0     ; acceleration height (patched earlier)
        ld      b,b     ; switch off accelerator
        pop     bc      ; BC=width
        pop     hl
        ld      e,l     ; E=y
        ld      a,(iy+usr_gmode)
        and     3               ; get ROP mode
        jr      nz,gputrop      ; move on if not simple copy case

        ; This is the simple copy case

        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x

        ; So, at this point we have:
        ;  HL=x, BC'=source, E=y, BC=width, (gput+1)=height

.gput3  exx
        ld      a,h
        out     (seg1),a        ; page in original seg1 for source
        ld      a,safe_yport
        out     (yport),a 
        ld      l,l     ; select accelerator memory blt mode
        ld      a,(bc)  ; get line of source
        ld      b,b     ; switch off accelerator
.gput4  ld      a,0     ; height of line (patched earlier)
        and     a
        jr      z,gput5
        add     a,c
        ld      c,a     ; add height to source address
        jr      nc,gput6
.gput5  inc     b
.gput6  exx
        ld      a,vpage
        out     (seg1),a        ; page in video memory
        ld      a,e
        out     (yport),a       ; set Y position
        ld      a,a     ; select accelerator screen blt mode
        ld      (hl),a  ; place line in destination
        ld      b,b     ; switch off accelerator
        inc     hl
        dec     bc
        ld      a,b
        or      c
        jr      nz,gput3
.gputend
        exx
        ld      a,h
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE

        ; This is the ROP case

.gputrop
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gput7
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gput7
        ld      a,$a6           ; AND (HL)
.gput7  ld      (gputop),a      ; store logic op

        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x

        ; So, at this point we have:
        ;  HL=x, BC'=source, E=y, BC=width, (gput+1)=height

.gput8  exx
        ld      a,h
        out     (seg1),a        ; page in original seg1 for source
        ld      a,safe_yport
        out     (yport),a 
        ld      l,l     ; select accelerator memory blt mode
        ld      a,(bc)  ; get line of source
        ld      b,b     ; switch off accelerator
.gput9  ld      a,0     ; height of line (patched earlier)
        and     a
        jr      z,gput10
        add     a,c
        ld      c,a     ; add height to source address
        jr      nc,gput11
.gput10 inc     b
.gput11 exx
        ld      a,vpage
        out     (seg1),a        ; page in video memory
        ld      a,e
        out     (yport),a       ; set Y position
        ld      a,a     ; select accelerator screen blt mode
.gputop nop             ; replaced by AND/OR/XOR (HL)
        ld      b,b     ; switch off accelerator
        ld      a,e
        out     (yport),a       ; reset Y position
        ld      a,a     ; select accelerator screen blt mode
        ld      (hl),a  ; place line in destination
        ld      b,b     ; switch off accelerator
        inc     hl
        dec     bc
        ld      a,b
        or      c
        jr      nz,gput8
        jr      gputend


;- GGET             x y w h addr --             Upload rectangle (assumes 320x256)
.grph_gget_entry
        ld      a,(iy+usr_gmode)
        and     3               ; get ROP mode
        jr      z,gget7         ; copy is NOP
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gget7
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gget7
        ld      a,$a6           ; AND (HL)
.gget7  ld      (ggetop),a      ; store logic op
        ld      h,b
        ld      l,c     ; HL=addr
        in      a,(seg1)
        ld      b,a     ; B=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        exx
        pop     de      ; E=height
        ld      a,e
        ld      (gget2+1),a    ; store height for accelerator initialisation
        ld      (gget4+1),a    ; and for source delta
        pop     bc      ; BC=width
        pop     hl
        ld      e,l     ; E=y
        pop     hl      ; HL=x
        res     7,h
        set     6,h     ; ensure seg 1 addressing for x

        ; So, at this point we have:
        ;  HL=x, HL'=dest, E=y, BC=width, (gget+1)=height

        ld      d,d     ; enable accelerator
.gget2  ld      a,0     ; acceleration height (patched earlier)
        ld      b,b     ; switch off accelerator
.gget3  ld      a,vpage
        out     (seg1),a        ; page in video memory
        ld      a,e
        out     (yport),a       ; set Y position
        ld      a,a     ; select accelerator screen blt mode
        ld      a,(hl)  ; get line from screen
        ld      b,b     ; switch off accelerator
        inc     hl
        exx
        ld      a,b
        out     (seg1),a        ; page in original seg1 for dest
        ld      a,safe_yport
        out     (yport),a 
        ld      l,l     ; select accelerator memory blt mode
.ggetop nop             ; AND/OR/XOR ought to go here
        ld      (hl),a  ; store line in dest
        ld      b,b     ; switch off accelerator
.gget4  ld      a,0     ; height of line (patched earlier)
        and     a
        jr      z,gget5
        add     a,l
        ld      l,a     ; add height to dest address
        jr      nc,gget6
.gget5  inc     h
.gget6  exx
        dec     bc
        ld      a,b
        or      c
        jr      nz,gget3

        exx
        ld      a,b
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE


;- GFLOOD           x y --                      Flood fill
.grph_gflood_entry
        ld      a,(iy+usr_gmode)
        and     3               ; get ROP mode
        jr      z,gfld1         ; copy is NOP
        ld      l,a
        ld      a,$b6           ; OR (HL)
        dec     l
        jr      z,gfld1
        ld      a,$ae           ; XOR (HL)
        dec     l
        jr      z,gfld1
        ld      a,$a6           ; AND (HL)
.gfld1  ld      (gfldop),a      ; store logic op
        pop     hl              ; HL=x
        res     7,h
        set     6,h             ; ensure seg1 addressing for X
        push    bc
        in      a,(seg1)
        ld      b,a             ; B=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        exx
        bit     4,(iy+usr_gmode)
        jr      z,gfld2         ; move on to set up solid colour fill
        ld      a,(iy+usr_gpattern)     ; A=pattern
        and     3
        add     a,a
        add     a,a
        add     a,a
        add     a,vidram_pattern0~$ff
        ld      d,vidram_pattern0/$100
        ld      e,a             ; DE=pattern source, column 0
        ld      c,7             ; C=pattern width-1
        jr      gfld3
.gfld2  ld      a,(iy+usr_gcolour)      ; A=colour
        ld      de,vidram_fillbuf   ; DE=solid pattern source
        ld      c,0             ; C=pattern width -1
        ld      e,e             ; set accelerator fill screen mode
        ld      (de),a          ; fill buffer with colour
        ld      b,b             ; switch off accelerator
.gfld3  pop     hl
        ld      b,l             ; B=y

        ; By now we have set up the fill pattern/solid and the required ROP
        ; HL'=x, DE=pattern source, C=pattern width, B=y

        exx
        push    bc              ; save registers
        push    de
        push    hl
.gfld4  bit     5,h
        jr      nz,gfld5        ; stop going left if x < 0
        call    floodline       ; flood one line
        dec     hl
        jr      c,gfld4         ; keep going if successful
.gfld5  pop     hl              ; restore original x
.gfld6  inc     hl
        ex      de,hl
        ld      hl,$413f
        and     a
        sbc     hl,de
        ex      de,hl
        jr      c,gfld7         ; stop going right if x > 319
        call    floodline       ; flood one line
        jr      c,gfld6         ; keep going if successful
.gfld7  pop     de              ; restore registers
        pop     bc
        ld      a,b
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE

        ; Subroutine to flood a single vertical line
        ; HL=x, DE'=pattern source, C'=pattern width, B'=y, other registers free
        ; On exit, Fc=1 if any pixels were flooded

.floodline
        xor     a
        out     (yport),a       ; set Y=0
        ld      de,floodbuf
        ld      d,d
        ld      a,0             ; set accelerator height 256
        ld      a,a             ; set accelerator screen blt mode
        ld      a,(hl)          ; get line from screen
        ld      l,l             ; set accelerator memory copy mode
        ld      (de),a          ; store line into memory
        ld      b,b             ; switch off accelerator
        exx
        ld      a,b             ; A=y
        exx
        push    hl              ; save x
        ld      d,a             ; D=y
        ld      bc,0            ; BC=height so far
        ld      hl,floodbuf
        add     a,l
        ld      l,a
        jr      nc,fldl1
        inc     h               ; HL=address of pixel at "y"
.fldl1  ld      e,(iy+usr_gcoltest)     ; E=test colour
        push    de              ; save y
        push    hl              ; save address of pixel at "y"
        bit     3,(iy+usr_gmode)
        jr      nz,fldld1       ; move on if testing for different colour
.fldls1 ld      a,(hl)
        cp      e
        jr      nz,fldls2       ; fail if pixel is different colour
        inc     hl              ; increment address, Y and match count
        inc     bc
        inc     d
        jr      nz,fldls1       ; until Y=256
.fldls2 pop     hl              ; restore address of pixel at "y"
        pop     de              ; restore y
        inc     d
.fldls3 dec     hl
        dec     d
        jr      z,fldl2         ; done if Y=0
        ld      a,(hl)
        cp      e
        jr      nz,fldl2        ; fail if pixel is different colour
        inc     bc              ; increment match count
        jr      fldls3
.fldld1 ld      a,(hl)
        cp      e
        jr      z,fldld2        ; fail if pixel is same colour
        inc     hl              ; increment address, Y and match count
        inc     bc
        inc     d
        jr      nz,fldld1       ; until Y=256
.fldld2 pop     hl              ; restore address of pixel at "y"
        pop     de              ; restore y
        inc     d
.fldld3 dec     hl
        dec     d
        jr      z,fldl2         ; done if Y=0
        ld      a,(hl)
        cp      e
        jr      z,fldl2         ; fail if pixel is same colour
        inc     bc              ; increment match count
        jr      fldld3
.fldl2  ld      a,b
        or      c
        jr      z,fldlfail      ; if no match, go to finish
        ld      a,c
        ld      (fldl3+1),a     ; patch accelerator height
        ld      d,d
.fldl3  ld      a,0             ; set accelerator height (patched above)
        ld      b,b             ; switch off accelerator
        ld      a,d
        out     (yport),a       ; set Y position
        pop     hl              ; restore X
        ld      a,l
        exx
        and     c
        add     a,e
        ld      h,d
        ld      l,a             ; form address of correct pattern line
        ld      a,a             ; set accelerator screen blt mode
        ld      a,(hl)          ; get line of pattern
        ld      b,b             ; switch off accelerator
        exx
        ld      a,d
        out     (yport),a       ; set Y position
        ld      a,a             ; set accelerator screen blt mode
.gfldop nop                     ; replaced by NOP or AND/OR/XOR (HL)
        ld      b,b             ; switch off accelerator
        ld      a,d
        out     (yport),a       ; set Y position
        ld      a,a             ; set accelerator screen blt mode
        ld      (hl),a          ; put line to screen
        ld      b,b             ; switch off accelerator
        scf                     ; Fc=1, success!
        ret
.fldlfail
        pop     hl              ; restore X
        and     a               ; Fc=0, failed
        ret


;- SETFONT          addr width type --          Download font as current font
;  "width" is now taken to be fixed width value; all fonts must start with
;  256 width entries. It will be divided by 2 for 640-mode.
.grph_setfont_entry
        pop     hl
        ld      a,l
        and     a
        jr      nz,sfnt18
        ld      l,8             ; default fixed-width=8
.sfnt18 ld      a,c
        and     a
        jr      z,sfnt1         
        inc     l
        srl     l               ; for 640-mode fonts, halve fixed width
.sfnt1  ld      b,l             ; B=fixed width, C=font type
        exx
        pop     de              ; DE'=address
        exx
        in      a,(seg1)
        ld      h,a             ; H=original seg1
        push    de              ; save IP
        push    iy              ; save UP
        ld      e,(iy+usr_activewin)
        ld      d,(iy+usr_activewin+1)
        push    de
        pop     iy              ; IY=window address
        ld      de,data_fixedwidths
        ld      a,(iy+sp_font)
        add     a,e
        ld      e,a
        jr      nc,sfnt2
        inc     d
.sfnt2  ld      a,b
        ld      (de),a          ; store fixed font width
        ld      d,d             ; set accelerator size=256 for glyph widths
        ld      a,0
        ld      b,b             ; switch off accelerator
        out     (yport),a       ; set Y=0
        ld      d,vidram_fontwidths/$100
        ld      a,vidram_fontwidths~$ff
        add     a,(iy+sp_font)
        ld      e,a             ; DE=font width address
        exx
        ld      l,l             ; set accelerator memory blt mode
        ld      a,(de)          ; load font widths to accelerator
        ld      b,b             ; switch off accelerator
        inc     d
        exx
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        ld      a,a             ; set accelerator screen blt mode
        ld      (de),a          ; store font widths to offscreen
        ld      b,b             ; switch off accelerator

        ; So, at this point we have loaded all the glyph widths to offscreen memory
        ; C=font type, DE=font widths address, L=original Y port, H=original seg 1
        ; DE'=address of font data (after widths)

        ld      b,0             ; 256 glyphs to download
.sfnt3
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        xor     a
        sub     b
        out     (yport),a
        ld      a,(de)          ; A=actual width of current glyph
        exx
        ld      l,a
        ld      h,0
        add     hl,de
        push    hl              ; TOS=address of next glyph
        exx
        ex      af,af'
        ld      a,c             ; check font type for clipping
        and     a
        jr      z,sfnt8
        ld      a,(de)
        inc     a
        srl     a               ; for 640-mode fonts, halve stored width
        ld      (de),a
        ex      af,af'
        cp      17
        jr      c,sfnt15        ; maximum width is 16
        ld      a,8
        ld      (de),a          ; limit stored bytewidth to 8
        add     a,a             ; and pixel width to 16
        jr      sfnt15
.sfnt8  ex      af,af'
        cp      9
        jr      c,sfnt15        ; maximum width is 8
        ld      a,8
        ld      (de),a          ; limit stored bytewidth & pixel width to 8
.sfnt15 and     a
        jp      z,sfnt16        ; skip zero-width glyphs
        ex      af,af'          ; save width of glyph to download
        ld      a,h
        out     (seg1),a        ; bind in source
        ld      a,safe_yport
        out     (yport),a 
        xor     a
        sub     b
        exx
        ld      c,a             ; C=glyph number
        and     7
        add     a,a
        add     a,a
        add     a,a             ; A=8*(glyph MOD 8)
        add     a,(iy+sp_fontaddr)
        ld      l,a
        ld      h,(iy+sp_fontaddr+1)    ; HL=address for this glyph
        ld      a,c
        and     248
        ld      c,a             ; C=line for this glyph
        ex      af,af'          ; A=glyph width
.sfnt6  ex      af,af'
        push    hl              ; save glyph offscreen address & line
        push    bc
        ld      hl,data_glyphbuf ; 8-byte buffer for line of glyph
        push    hl
        exx
        ld      a,c             ; get font type
        exx
        and     a
        ld      a,(de)          ; get next line of glyph data
        inc     de
        jr      z,sfnt7         ; move on for 320-resolution fonts

        ; 640-mode glyph conversion

        ld      b,8
        ld      c,a
.sfnt9  xor     a               ; "background" for left & right columns
        rl      c
        jr      nc,sfnt10
        ld      a,$f0           ; "foreground" for left column
.sfnt10 ld      (hl),a          ; set buffer byte of left column
        inc     hl
        djnz    sfnt9           ; fill rest of buffer with line of glyph
        ex      af,af'
        dec     a
        jr      z,sfnt11        ; move on if no right column to fill
        ex      af,af'
        pop     hl              ; restore buffer address
        push    hl
        ld      a,(de)          ; get next line of glyph data
        inc     de
        ld      b,8
        ld      c,a
.sfnt12 ld      a,(hl)          ; get buffer byte
        rl      c
        jr      nc,sfnt13
        or      $0f             ; "foreground" for right column
        ld      (hl),a          ; update buffer byte for right column
.sfnt13 inc     hl
        djnz    sfnt12          ; fill rest of buffer with line of glyph
        jr      sfnt14          ; go to do line download
.sfnt11 ld      a,1             ; ensure the loop finishes this time around
        ex      af,af'
        jr      sfnt14          ; go to do line download

        ; 320-mode glyph conversion

.sfnt7  ld      b,8
.sfnt4  ld      c,$00           ; "background"
        rla                     ; check next bit of glyph
        jr      nc,sfnt5
        dec     c               ; "foreground"
.sfnt5  ld      (hl),c          ; set buffer byte
        inc     hl
        djnz    sfnt4           ; fill rest of buffer with line of glyph

.sfnt14 pop     hl
        ld      d,d             ; set accelerator length=8
        ld      a,8
        ld      l,l             ; set accelerator memory blt mode
        ld      a,(hl)          ; load line of glyph to accelerator
        ld      b,b             ; switch off accelerator
        pop     bc              ; restore glyph offscreen address and line
        pop     hl
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        ld      a,c
        out     (yport),a       ; set glyph line
        ld      a,a             ; set accelerator screen blt mode
        ld      (hl),a          ; store line of glyph to offscreen
        ld      b,b             ; switch off accelerator
        inc     hl              ; increment offscreen glyph address
        exx
        ld      a,h
        out     (seg1),a        ; bind back source
        ld      a,safe_yport
        out     (yport),a 
        exx
        ex      af,af'
        dec     a
        jr      nz,sfnt6        ; loop back for rest of glyph
.sfnt17 pop     de              ; restore calculated address of next glyph
        exx
        dec     b
        jp      nz,sfnt3        ; loop back for rest of glyphs
        exx
        ld      a,safe_yport
        out     (yport),a       ; restore YPORT
        pop     iy              ; restore UP
        pop     de              ; restore IP
        pop     bc              ; get new TOS
        jp      DONE
.sfnt16 ld      a,h
        out     (seg1),a        ; bind back source
        exx
        jr      sfnt17


;- SETPATTERN       addr --                     Download pattern as current pattern
.grph_setpattern_entry
        in      a,(seg1)
        ex      af,af'          ; A'=original seg1
        push    bc
        exx
        pop     de              ; DE=pattern source
        ld      a,(iy+usr_gpattern)
        and     3
        add     a,a
        add     a,a
        add     a,a
        add     a,vidram_pattern0~$ff
        ld      h,vidram_pattern0/$100
        ld      l,a             ; HL=offscreen pattern address
        ld      d,d
        ld      a,8             ; set acceleration height 8
        ld      b,b             ; switch off accelerator
        ld      b,8             ; 8 lines
.setp1  ld      l,l             ; select accelerator memory blt mode
        ld      a,(de)          ; get line of source
        ld      b,b             ; switch off accelerator
        ld      a,e
        add     a,8             ; advance source pointer
        ld      e,a
        jr      nc,setp2
        inc     d
.setp2  ld      a,vpage
        out     (seg1),a        ; page in video memory
        xor     a               ; start at top, doing 8 pixels at a time
.setp3  out     (yport),a       ; set Y position
        ld      a,a             ; select accelerator screen blt mode
        ld      (hl),a          ; place line in destination
        ld      b,b             ; switch off accelerator
        add     a,8
        jr      nz,setp3        ; fill an entire line with pattern
        inc     hl
        ex      af,af'
        out     (seg1),a        ; page back original source
        ex      af,af'
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        djnz    setp1
        exx
        pop     bc              ; get new TOS
        jp      DONE


; Subroutine to find pixel address HL=x, BC=y
; Returns HL=address, A=pixel, C=segment 1 binding
; and segment 1 bound to graphics page
; Sets carry if pixel out of range (B/C not valid)
; Enter at pixnob to avoid point being fixed to window bounds when
; GMODE bit 2 set

.pixadd bit     2,(iy+usr_gmode)
        jr      z,pixnob        ; move on if not forcing points in bounds
        ld      a,h
        cp      2
        jr      nc,pixxbad      ; X is definitely bad (>=512, or negative)
        and     a
        jr      z,pixxok        ; X is definitely good (<256)
        ld      a,l
        cp      64
        jr      c,pixxok        ; X is good (<320)
.pixxbad
        rlc     h
        ld      hl,0            ; fix to 0 if negative
        jr      c,pixxok
        ld      hl,319          ; or 319 if >319
.pixxok ld      a,b
        and     a
        jr      z,pixnob        ; Y is good (<256)
        rlca
        ld      bc,0            ; fix to 0 if negative
        jr      c,pixnob
        ld      bc,255          ; or 255 if >255
.pixnob ld      (iy+usr_coords),c ; set last point plotted (even if out of range)
        ld      (iy+usr_coords+1),b
        ld      (iy+usr_coords+2),l
        ld      (iy+usr_coords+3),h
        ld      a,h
        and     254
        or      b
        scf
        ret     nz              ; exit with Fc=1 if X>511 or Y>255, or -ve
        ld      a,h
        and     1
        jr      z,goodpix       ; okay if X<256
        ld      a,l
        cp      64
        ccf
        ret     c               ; exit with Fc=1 if X>319
.goodpix
        res     7,h
        set     6,h             ; convert X coordinate to seg 1 addressing
        ld      a,c
        out     (yport),a       ; set Y coordinate
        in      a,(seg1)
        ld      c,a             ; save original seg1
        ld      a,vpage
        out     (seg1),a        ; page in VRAM
        xor     a               ; Fc=0, Fz=1
        ret


; Subroutine to affect current pixel according to GMODE and GCOLOUR/GPATTERN

.dopix  ld      a,(iy+usr_gmode)
        bit     4,a
        jr      nz,dopixpattern
        and     3               ; get ROP mode
        jr      z,dopixcopy
        dec     a
        jr      z,dopixor
        dec     a
        jr      z,dopixxor
.dopixand
        ld      a,(iy+usr_gcolour)
        and     (hl)
        ld      (hl),a
        ret
.dopixxor
        ld      a,(iy+usr_gcolour)
        xor     (hl)
        ld      (hl),a
        ret
.dopixor
        ld      a,(iy+usr_gcolour)
        or      (hl)
        ld      (hl),a
        ret
.dopixcopy
        ld      a,(iy+usr_gcolour)
        ld      (hl),a
        ret
.dopixpattern
        push    de
        push    hl
        and     3
        ld      e,a             ; save ROP mode
        ld      a,(iy+usr_gpattern)
        and     3
        add     a,a
        add     a,a
        add     a,a
        add     a,vidram_pattern0~$ff
        ld      d,a             ; D=low part of start of offscreen pattern
        ld      a,l
        and     7
        add     a,d             ; offset to correct line
        ld      h,vidram_pattern0/$100 
        ld      l,a             ; HL=offscreen pattern address
        ld      a,(hl)          ; get pattern colour
        pop     hl
        inc     e
        dec     e
        jr      z,dopixpatterncopy
        dec     e
        jr      z,dopixpatternor
        dec     e
        jr      z,dopixpatternxor
.dopixpatternand
        and     (hl)
        ld      (hl),a
        pop     de
        ret
.dopixpatternxor
        xor     (hl)
        ld      (hl),a
        pop     de
        ret
.dopixpatternor
        or      (hl)
        ld      (hl),a
        pop     de
        ret
.dopixpatterncopy
        ld      (hl),a
        pop     de
        ret


; Subroutine to draw a line from last coordinates across HL=X, up BC=Y

.drawline
	ld	a,h		; check if ABS X < 256
	and	a
	jr	z,drawlinechecky
	inc	a
	jr	nz,drawlinelong
.drawlinechecky
	ld	a,b		; check if ABS Y < 256
	and	a
	jr	z,drawline255
	inc	a
	jr	z,drawline255
.drawlinelong
	; Shitsticks. The line is too long in at least one direction,
	; so draw it in two halves.
	push	hl		; save original lengths
	push	bc
	sra	h		; halve X
	rr	l
	sra	b		; halve Y
	rr	c
	call	drawline	; draw 1st half
	pop	hl
	ld	b,h
	ld	c,l
	sra	b
	rr	c
	and	a
	sbc	hl,bc
	ld	b,h
	ld	c,l		; Y=Y-(Y/2)
	pop	hl
        ld      d,h
        ld      e,l
	sra	d
	rr	e
	and	a
	sbc	hl,de		; X=X-(X/2)
	jr	drawline	; re-enter with 2nd half in case still too long
.drawline255
        ld      a,b
        rlca
        ld      a,c
        ld      d,1
        jr      nc,gline1       ; move on if Y positive
        neg
        ld      d,-1
.gline1 ld      b,a             ; B=ABS Y, D=SGN Y
        ld      a,c
        and     a
        jr      nz,glin1a
        ld      d,0             ; if ABS Y=0, SGN Y=0
.glin1a ld      a,h
        rlca
        ld      a,l
        ld      e,1
        jr      nc,gline2       ; move on if X positive
        neg
        ld      e,-1
.gline2 ld      c,a             ; C=ABS X, E=SGN X
        ld      a,l
        and     a
        jr      nz,glin2a
        ld      e,0             ; if ABS X=0, SGN X=0

; By now:
;  B=ABS Y (0..255)
;  D=SGN Y (-1,0,1)
;  C=ABS X (0..255)
;  E=SGN X (-1,0,1)

.glin2a ld      a,c
        cp      b
        jr      nc,gline3       ; find smaller of ABS X and ABS Y
        ld      l,c             ; ABS X is smaller
        push    de              ; save diagonal step
        ld      e,0             ; use vertical step
        jr      gline4
.gline3 or      b
        ret     z               ; exit if nothing to do
        ld      l,b             ; ABS Y is smaller
        ld      b,c             ; ABS X is larger 
        push    de              ; save diagonal step
        ld      d,0             ; use horizontal step
.gline4 ld      h,b             ; largest of ABS X/ABS Y
        ld      a,b
        rra

; By now:
; TOS=diagonal step (D=SGN Y, E=SGN X)
;   L=smaller of ABS X/ABS Y (0..255)
;   B=larger of ABS X/ABS Y (0..255)
;  DE=horizontal/vertical step (0,-1|1) or (-1|1,0)
;   H=B as well, and A=(rr B)

; End of setup, now loop starts

.gline5 add     a,l
        jr      c,gline6
        cp      h
        jr      c,gline7
.gline6 sub     h
        ld      c,a
        exx
        pop     bc              ; BC'=diagonal step
        push    bc
        jr      gline8
.gline7 ld      c,a
        push    de
        exx
        pop     bc              ; BC'=hor/vert step
.gline8 ld      l,(iy+usr_coords)       ; HL=last Y
        ld      h,(iy+usr_coords+1)
        ld      d,b
        dec     d
        jr      z,glinec
        inc     d
.glinec ld      e,b             ; DE=vertical step (0, -1 or 1)
        add     hl,de
        ld      d,c
        dec     d
        jr      z,glined
        inc     d
.glined ld      e,c             ; DE=horizontal step (0, -1 or 1)
        ld      b,h
        ld      c,l             ; BC=new Y
        ld      l,(iy+usr_coords+2)     ; HL=last X
        ld      h,(iy+usr_coords+3)
        add     hl,de           ; HL=new X
        call    pixadd
        jr      c,glinea        ; don't plot if out of range
        call    dopix           ; plot point
        ld      a,c
        out     (seg1),a        ; rebind segment 1
        ld      a,safe_yport
        out     (yport),a       ; restore YPORT
.glinea exx
        ld      a,c
        djnz    gline5
        pop     de              ; discard diagonal step
        ret


;- GPIXEL           x y --                      Change pixel at (x,y) according to GMODE
.grph_gpixel_entry
        pop     hl              ; HL=x, BC=y
        call    pixadd
        jr      c,pset1         ; no effect if out of range
        call    dopix
        ld      a,c
        out     (seg1),a        ; rebind segment 1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
.pset1  pop     bc              ; get new TOS
        jp      DONE


;- GPIXEL?          x y -- colour               Return colour of pixel at (x,y)
;                                               or -1 if pixel out of range
.grph_gpixelq_entry
        pop     hl              ; HL=x, BC=y
        call    pixadd
        jr      c,gpq2          ; special case if out of range
        ld      l,(hl)          ; L=colour
        ld      a,c
        out     (seg1),a        ; rebind segment 1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        ld      b,0
        ld      c,l             ; TOS=pixel colour
        jp      DONE
.gpq2   ld      bc,-1           ; out of range
        jp      DONE


;- GLINE            x y --                      Draw line from last plot across X, up Y
.grph_gline_entry
        pop     hl              ; HL=X, BC=Y
        push    de              ; save IP
        call    drawline
        pop     de              ; restore IP
        pop     bc              ; new TOS
        jp      DONE


;- GATTRIB          attr --                     Sets graphical mode text colours
;NB: this isn't a complete word, but called at the end of ATTRIB
.grph_gattrib_entry
        ld      a,c
        cpl
        ld      c,a             ; C=foreground buffer value
        xor     b
        ld      b,a             ; B=background buffer value
        push    bc
        in      a,(seg1)
        ld      h,a     ; H=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        exx
        pop     bc
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        ld      a,(hl)          ; A=current window number
        add     a,a
        ld      hl,vidram_bgbuffer
        add     a,l
        ld      l,a
        jr      nc,gattr1
        inc     h       ; HL=address of bg buffer for this window
.gattr1 ld      d,d
        ld      a,0     ; acceleration height 256
        ld      e,e     ; set accelerator fill vertical line mode
        ld      (hl),b  ; fill background buffer
        inc     hl
        ld      (hl),c  ; fill foreground buffer
        ld      b,b     ; switch off accelerator
        exx
        ld      a,h
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc      ; get new TOS
        jp      DONE


;- FONT             n --                        Set current font (0-MAXFONT) for current window
.grph_font_entry
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        push    iy              ; save IY
        push    hl
        pop     iy              ; IY=window address
        ld      a,MAXFONT
        sub     c
        jr      nc,font2
        xor     a               ; limit font number to MAXFONT, also ensure Fc=0
.font2  ld      (iy+sp_font),a  ; font=MAXFONT-font, so font 0 goes rightmost in memory
        rra
        rra
        rra                     ; FcA=64*font
        ld      c,a
        ld      a,0
        rla
        ld      b,a             ; BC=64*font
        ld      hl,vidram_fonts
        add     hl,bc           ; HL=font address in offscreen
        ld      (iy+sp_fontaddr),l
        ld      (iy+sp_fontaddr+1),h
        pop     iy              ; restore IY
        pop     bc              ; get new TOS
        jp      DONE


;- SETPAL           a u1 u2 n --                Set u2 entries of palette n from entry u1, stored at a
.grph_setpal_entry
        in      a,(seg1)
        ex      af,af'          ; A'=original seg1
        ld      a,c
        and     7               ; A=palette number 0..7
        exx
        ld      hl,vidram_palettes
        add     a,a
        add     a,a
        add     a,l
        ld      l,a
        jr      nc,setpl1
        inc     h               ; HL=address of palette in VRAM
.setpl1 pop     bc
        ld      a,c
        pop     bc
        ld      b,a             ; B=u2 (# entries), C=u1 (first entry)
        pop     de              ; DE=palette data (4 bytes per entry)
        push    ix              ; save IX & IY
        push    iy
.setpl2 ld      a,(de)          ; get 4 bytes of palette data
        ld      ixl,a
        inc     de
        ld      a,(de)
        ld      ixh,a
        inc     de
        ld      a,(de)
        ld      iyl,a
        inc     de
        ld      a,(de)
        ld      iyh,a
        inc     de
        ld      a,c
        out     (yport),a       ; set line of palette entry
        ld      a,vpage
        out     (seg1),a        ; page in VRAM
        push    hl
        ld      a,ixl           ; store 4 bytes of palette data
        ld      (hl),a
        inc     hl
        ld      a,ixh
        ld      (hl),a
        inc     hl
        ld      a,iyl
        ld      (hl),a
        inc     hl
        ld      a,iyh
        ld      (hl),a
        pop     hl
        ex      af,af'
        out     (seg1),a        ; rebind seg 1
        ex      af,af'
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        inc     c               ; move to next palette entry
        djnz    setpl2          ; back for rest
        pop     iy              ; restore regs
        pop     ix
        exx
        pop     bc
        jp      DONE


;- GETPAL           a u1 u2 n --                Get u2 entries of palette n from entry u1, stored at a
.grph_getpal_entry
        in      a,(seg1)
        ex      af,af'          ; A'=original seg1
        ld      a,c
        and     7               ; A=palette number 0..7
        exx
        ld      hl,vidram_palettes
        add     a,a
        add     a,a
        add     a,l
        ld      l,a
        jr      nc,getpl1
        inc     h               ; HL=address of palette in VRAM
.getpl1 pop     bc
        ld      a,c
        pop     bc
        ld      b,a             ; B=u2 (# entries), C=u1 (first entry)
        pop     de              ; DE=palette data (4 bytes per entry)
        push    ix              ; save IX & IY
        push    iy
.getpl2 ld      a,c
        out     (yport),a       ; set line of palette entry
        ld      a,vpage
        out     (seg1),a        ; page in VRAM
        ld      b,b             ; make sure accelerator is off
        push    hl
        ld      a,(hl)          ; get 4 bytes of palette data
        ld      ixl,a
        inc     hl
        ld      a,(hl)
        ld      ixh,a
        inc     hl
        ld      a,(hl)
        ld      iyl,a
        inc     hl
        ld      a,(hl)
        ld      iyh,a
        pop     hl
        ex      af,af'
        out     (seg1),a        ; rebind seg 1
        ex      af,af'
        ld      a,safe_yport
        out     (yport),a
        ld      a,ixl           ; store 4 bytes of palette data
        ld      (de),a
        inc     de
        ld      a,ixh
        ld      (de),a
        inc     de
        ld      a,iyl
        ld      (de),a
        inc     de
        ld      a,iyh
        ld      (de),a
        inc     de
        inc     c               ; move to next palette entry
        djnz    getpl2          ; back for rest
        pop     iy              ; restore regs
        pop     ix
        exx
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        pop     bc
        jp      DONE


;- GMOVE            x y --                      Move plot position to (x,y)
.grph_gmove_entry
        pop     hl
        ld      (iy+usr_coords),c
        ld      (iy+usr_coords+1),b
        ld      (iy+usr_coords+2),l
        ld      (iy+usr_coords+3),h
        pop     bc              ; new TOS
        jp      DONE


;- GPOS?            -- x y                      Get plot position
.grph_gposq_entry
        push    bc              ; save old TOS
        ld      l,(iy+usr_coords+2)
        ld      h,(iy+usr_coords+3)
        push    hl
        ld      c,(iy+usr_coords)
        ld      b,(iy+usr_coords+1)
        jp      DONE


;- WIDTH            a u1 -- u2                  Get width of text string in current font
.grph_width_entry
        in      a,(seg1)
        ex      af,af'          ; A'=original seg1
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        push    bc
        exx
        pop     bc              ; BC=string count
        pop     de              ; HL=string address
        ld      l,(iy+usr_activewin)
        ld      h,(iy+usr_activewin+1)
        push    iy              ; save IY
        push    hl
        pop     iy              ; IY=window address
        ld      h,vidram_fontwidths/$100
        ld      a,vidram_fontwidths~$ff
        add     a,(iy+sp_font)
        ld      l,a             ; HL=font width address
        ld      iy,0            ; total width=0
.width1 ld      a,b
        or      c
        jr      z,widthdone
        ld      a,(de)          ; get next char
        inc     de
        dec     bc
        out     (yport),a       ; set YPORT
        ld      a,vpage
        out     (seg1),a
        ld      a,(hl)          ; A=width of glyph
        add     a,iyl           ; add in
        ld      iyl,a
        jr      nc,width2
        inc     iyh
.width2 ld      a,safe_yport
        out     (yport),a
        ex      af,af'
        out     (seg1),a        ; rebind seg1
        jr      width1
.widthdone
        ex      (sp),iy         ; restore IY, stack result
        exx
        pop     bc              ; TOS=result
        ld      a,safe_yport
        out     (yport),a
        jp      DONE


; Graphics character output routine
; On entry, IY=window address, C=char.
; All registers may be corrupted. Exit via RET, not DONE
; Horribles will occur if window size isn't enough to output the glyph!
.grph_charout_entry
        in      a,(seg1)
        ld      h,a             ; H=original seg1
        push    hl              ; save them
        ld      a,vpage
        out     (seg1),a        ; bind in VRAM
        ld      a,c             ; A=char
        cp      12
        jp      z,grph_char_clear
        cp      13
        jp      z,grph_char_newline
        out     (yport),a       ; set Y to glyph number
        bit     6,(iy+sp_mode)  ; check for fixed-width font mode
        jp      nz,grph_charout_fixed
        ld      d,vidram_fontwidths/$100
        ld      a,vidram_fontwidths~$ff
        add     a,(iy+sp_font)
        ld      e,a             ; DE=font width address
        ld      a,(de)          ; A=width of glyph
        and     a
        jr      z,grph_char_done        ; do nothing if zero
        ld      b,a             ; B=glyph width

        ; Okay, now we have C=glyph number, B=glyph width
        ; We need to determine if this glyph will fit on the current line

        add     a,(iy+sp_cursorx)
        ld      e,a
        ld      a,0
        adc     a,(iy+sp_cursorx+1)
        ld      d,a             ; DE=window position after glyph
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)      ; HL=max usable x
        inc     hl              ; HL=max position after glyph
        and     a
        sbc     hl,de
        call    c,grph_newline  ; move onto next line if not enough room
        ld      a,(iy+sp_cursory)
        add     a,7
        call    c,grph_newline  ; move onto next line if (Y+7)>255
        ld      a,(iy+sp_cursory)
        add     a,6
        cp      (iy+sp_limity)
        call    nc,grph_newline ; move onto next line if (Y+6)>=last window line

        ; Now the main glyph rendering code

.glyph_render
        ld      l,(iy+sp_cursorx)
        ld      h,(iy+sp_cursorx+1)
        res     7,h
        set     6,h             ; ensure seg 1 addressing for X
        push    hl              ; TOS=X position
        ld      a,c
        and     7
        add     a,a
        add     a,a
        add     a,a             ; A=8*(glyph MOD 8)
        add     a,(iy+sp_fontaddr)
        ld      e,a
        ld      d,(iy+sp_fontaddr+1)    ; DE=address for this glyph
        ld      a,c
        and     248
        ld      c,a             ; C=line for this glyph
        ld      a,(iy+sp_number)        ; A=current window number
        add     a,a
        ld      hl,vidram_bgbuffer
        add     a,l
        ld      l,a
        jr      nc,gchar1
        inc     h               ; HL=address of bg buffer for this window
.gchar1 ld      d,d
        ld      a,8             ; acceleration height 8
        ld      b,b             ; switch off accelerator
.gchar2 ld      a,c
        out     (yport),a       ; set glyph Y
        ld      a,a             ; select accelerator screen blt mode
        ld      a,(de)          ; get line of glyph to accelerator
        inc     de
        or      (hl)            ; OR in background buffer
        inc     hl
        xor     (hl)            ; XOR in foreground buffer
        dec     hl
        ld      b,b             ; switch off accelerator
        ld      a,(iy+sp_cursory)
        out     (yport),a       ; set screen Y
        ex      (sp),hl         ; TOS=bg buffer, HL=x
        ld      a,a             ; select accelerator screen blt mode
        ld      (hl),a          ; put line of glyph on screen
        ld      b,b             ; switch off accelerator
        inc     hl
        ex      (sp),hl         ; TOS=x, HL=bg buffer
        djnz    gchar2          ; back for rest of glyph
        pop     hl              ; HL=final x
        res     6,h             ; remove segment 1 addressing
        ld      (iy+sp_cursorx),l
        ld      (iy+sp_cursorx+1),h     ; store final x position
.grph_char_done
        pop     hl              ; restore saved values
        ld      a,h
        out     (seg1),a        ; rebind seg1
        ld      a,safe_yport
        out     (yport),a       ; reset YPORT
        ret                     ; finished!!

        ; Special control character considerations

.grph_char_newline
        call    grph_newline            ; just do the newline
        jr      grph_char_done          ; and finish

.grph_char_clear
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)
        inc     hl
        ld      e,(iy+sp_originx)
        ld      d,(iy+sp_originx+1)
        ld      (iy+sp_cursorx),e
        ld      (iy+sp_cursorx+1),d     ; reset cursor column
        and     a
        sbc     hl,de                   ; HL=window width
        res     7,d
        set     6,d                     ; DE=window X, addressed to seg 1
        ld      a,(iy+sp_originy)
        ld      (iy+sp_cursory),a       ; reset cursor line
        ex      af,af'                  ; A'=top of window
        ld      a,(iy+sp_limity)
        inc     a
        sub     (iy+sp_originy)         ; A=window height
        ld      (gchar6+1),a            ; patch accelerator height
        ld      d,d
.gchar6 ld      a,0                     ; set accelerator height (patched)
        ld      b,b                     ; switch off accelerator
.gchar7 ex      af,af'
        out     (yport),a               ; set Y position
        ex      af,af'
        ld      a,(iy+sp_attr+1)        ; A=background colour
        ld      e,e                     ; set accelerator fill vertical line mode
        ld      (de),a                  ; fill line with colour
        ld      b,b                     ; switch off accelerator
        inc     de
        dec     hl
        ld      a,h
        or      l
        jr      nz,gchar7               ; back to fill rest of line
        jr      grph_char_done

        ; Subroutine to do a newline on the window, ensuring at
        ; least 1 8-pixel line can be output
        ; Must preserve A', BC & IY

.grph_newline
        ld      l,(iy+sp_originx)       ; reset cursor position to left
        ld      h,(iy+sp_originx+1)
        ld      (iy+sp_cursorx),l
        ld      (iy+sp_cursorx+1),h
        ld      a,(iy+sp_cursory)
        add     a,8                     ; A=top of next line
        jr      c,grph_winscroll        ; need to scroll if >255
        ld      e,a                     ; E=top of line coordinate
        add     a,7
        jr      c,grph_winscroll        ; need to scroll if no room on screen
        ld      d,a                     ; D=bottom of line coordinate
        ld      a,(iy+sp_limity)
        cp      d
        jr      c,grph_winscroll        ; need to scroll if no room in window
        ld      (iy+sp_cursory),e       ; update cursor position
        ret
.grph_winscroll
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)
        inc     hl
        ld      e,(iy+sp_originx)
        ld      d,(iy+sp_originx+1)
        and     a
        sbc     hl,de                   ; HL=window width
        res     7,d
        set     6,d                     ; DE=window X, addressed to seg 1
        push    hl                      ; save them for the fill
        push    de
        ld      a,(iy+sp_limity)
        inc     a
        sub     (iy+sp_originy)         ; A=height of window
        jr      z,grph_doscroll         ; do scroll if height=256 (if 0 we're screwed anyway)
        sub     8                       ; height of window to scroll
        jr      c,grph_noscroll         ; don't scroll if <=0
        jr      z,grph_noscroll
.gchar8 ld      (gchar3+1),a            ; patch accelerator height
        ld      d,d
.gchar3 ld      a,0                     ; set accelerator height (patched)
        ld      b,b                     ; switch off accelerator
.gchar4 ld      a,(iy+sp_originy)
        add     a,8
        out     (yport),a               ; set source Y to window top + 8
        ld      a,a                     ; set accelerator screen blt mode
        ld      a,(de)                  ; fetch line from window
        ld      b,b                     ; switch off accelerator
        ld      a,(iy+sp_originy)
        out     (yport),a               ; set dest Y to window top
        ld      a,a                     ; set accelerator screen blt mode
        ld      (de),a                  ; store line to window
        ld      b,b                     ; switch off accelerator
        inc     de
        dec     hl
        ld      a,h
        or      l
        jr      nz,gchar4               ; back for rest of window width
.grph_noscroll
        pop     de                      ; restore DE=X
        pop     hl                      ; restore HL=width
        ld      a,(iy+sp_limity)
        sub     7
        ld      (iy+sp_cursory),a
        ex      af,af'                  ; A'=top of new printing line
        push    af                      ; save A'
        ld      d,d
        ld      a,8                     ; set accelerator height to 8
        ld      b,b                     ; switch off accelerator
.gchar5 ex      af,af'
        out     (yport),a               ; set Y position
        ex      af,af'
        ld      a,(iy+sp_attr+1)        ; A=background colour
        ld      e,e                     ; set accelerator fill vertical line mode
        ld      (de),a                  ; fill line with colour
        ld      b,b                     ; switch off accelerator
        inc     de
        dec     hl
        ld      a,h
        or      l
        jr      nz,gchar5               ; back to fill rest of line
        pop     af                      ; restore A'
        ex      af,af'
        ret                             ; finished!!
.grph_doscroll
        sub     8
        jr      gchar8


; In forced-fixed-width mode, we ensure that each character is rendered
; at exactly a multiple of the width from the window's left edge

.grph_charout_fixed
        cp      8
        jp      z,grph_char_left
        cp      9
        jp      z,grph_char_right
        ld      de,data_fixedwidths
        ld      a,(iy+sp_font)
        add     a,e
        ld      e,a
        jr      nc,gcf1
        inc     d
.gcf1   ld      a,(de)
        ex      af,af'                  ; A'=font's fixed width
        ld      l,(iy+sp_cursorx)
        ld      h,(iy+sp_cursorx+1)
        ld      e,(iy+sp_originx)
        ld      d,(iy+sp_originx+1)
        and     a
        sbc     hl,de
        jp      c,forcenewline          ; if before the left edge, summat's up
        jr      z,forcealigned          ; we're already aligned, so no problem
        ex      af,af'
        ld      e,a
        ex      af,af'
        ld      d,0
.forceposloop
        sbc     hl,de
        jr      z,forcealigned          ; we're already aligned, so no problem
        jr      nc,forceposloop
        ld      a,l
        neg                             ; A=width to move on
        add     a,(iy+sp_cursorx)
        ld      e,a
        ld      a,0
        adc     a,(iy+sp_cursorx+1)
        ld      d,a                     ; DE=window position after alignment
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)      ; HL=max usable x
        inc     hl                      ; HL=max position after glyph
        and     a
        sbc     hl,de
        jr      c,forcenewline          ; not enough room for the alignment
        ld      (iy+sp_cursorx),e       ; update aligned x
        ld      (iy+sp_cursorx+1),d
.forcealigned                           ; now test if there's enough room for a character
        ex      af,af'
        ld      e,a
        ex      af,af'
        ld      a,e                     ; A=fixed glyph width
        add     a,(iy+sp_cursorx)
        ld      e,a
        ld      a,0
        adc     a,(iy+sp_cursorx+1)
        ld      d,a                     ; DE=window position after glyph
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)      ; HL=max usable x
        inc     hl                      ; HL=max position after glyph
        and     a
        sbc     hl,de
        call    c,grph_newline          ; move onto next line if not enough room

        ; Check if need to move to next line because of insufficient scanlines
        ; in window, but don't bother about alignment problems - that's the
        ; user's lookout for now

        ld      a,(iy+sp_cursory)
        add     a,7
        call    c,grph_newline  ; move onto next line if (Y+7)>255
        ld      a,(iy+sp_cursory)
        add     a,6
        cp      (iy+sp_limity)
        call    nc,grph_newline ; move onto next line if (Y+6)>=last window line

        ; Now output the glyph with space around it to add up to the
        ; fixed width (if larger)

        ld      h,vidram_fontwidths/$100
        ld      a,vidram_fontwidths~$ff
        add     a,(iy+sp_font)
        ld      l,a                     ; HL=font width address
        ld      a,(hl)                  ; A=glyph width
        ex      af,af'
        ld      b,a                     ; B=fixed glyph width, C=glyph number
        ex      af,af'
        sub     b
        jp      nc,glyph_render         ; if glyph>=fixed, use fixed and go

        ; Otherwise, at this point, A=-space we must output in addition to the
        ; glyph. For neatness, we should do half before and half after, but
        ; that would be tricky, so for now we'll just do it all before.

        ld      b,(hl)
        push    bc                      ; save glyph width and number
        neg
        ld      b,a                     ; B=spacing
        ld      e,(iy+sp_cursorx)
        ld      d,(iy+sp_cursorx+1)
        res     7,d
        set     6,d                     ; DE=x, addressed to seg 1
        ld      a,(iy+sp_cursory)
        ex      af,af'                  ; A'=y
        ld      d,d
        ld      a,8                     ; set accelerator height to 8
        ld      b,b                     ; switch off accelerator
.galgn2 ex      af,af'
        out     (yport),a               ; set Y position
        ex      af,af'
        ld      a,(iy+sp_attr+1)        ; A=background colour
        ld      e,e                     ; set accelerator fill vertical line mode
        ld      (de),a                  ; fill line with colour
        ld      b,b                     ; switch off accelerator
        inc     de
        djnz    galgn2
        ld      b,b                     ; switch off accelerator
        res     6,d                     ; unaddress X
        ld      (iy+sp_cursorx),e       ; update window
        ld      (iy+sp_cursorx+1),d
        pop     bc                      ; restore B=glyph width, C=glyph
        jp      glyph_render

.forcenewline
        call    grph_newline
        jr      forcealigned

        ; The cursor left/right code expects the position to already be
        ; aligned, and is relatively simple

.grph_char_left
        ld      hl,data_fixedwidths
        ld      a,(iy+sp_font)
        add     a,l
        ld      l,a
        jr      nc,gcl1
        inc     h
.gcl1   ld      c,(hl)                  ; BC=fixed character width
        ld      b,0
        ld      l,(iy+sp_cursorx)
        ld      h,(iy+sp_cursorx+1)
        ld      e,(iy+sp_originx)
        ld      d,(iy+sp_originx+1)
        and     a
        sbc     hl,bc                   ; left by 1 character width
        jr      c,gclup                 ; go up if beyond left of screen
        ld      (iy+sp_cursorx),l
        ld      (iy+sp_cursorx+1),h     ; update position
        sbc     hl,de
        jp      nc,grph_char_done       ; finished if within window
.gclup  ld      a,(iy+sp_cursory)
        sub     8
        ld      (iy+sp_cursory),a
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)
        inc     hl
        and     a
        sbc     hl,de                   ; HL=window width
.gclup1 sbc     hl,bc
        jr      z,gclup2                ; found alignment=0
        jr      nc,gclup1               ; keep going
        add     hl,bc                   ; HL=width to remove from limit
.gclup2 ex      de,hl
        ld      l,(iy+sp_limitx)
        ld      h,(iy+sp_limitx+1)
        inc     de
        and     a
        sbc     hl,de                   ; HL=effective limit
        sbc     hl,bc                   ; HL=start of last char
        ld      (iy+sp_cursorx),l       ; update position
        ld      (iy+sp_cursorx+1),h
        jp      grph_char_done

.grph_char_right
        ld      hl,data_fixedwidths
        ld      a,(iy+sp_font)
        add     a,l
        ld      l,a
        jr      nc,gcr1
        inc     h
.gcr1   ld      c,(hl)                  ; BC=fixed character width
        ld      b,0
        ld      l,(iy+sp_cursorx)
        ld      h,(iy+sp_cursorx+1)
        add     hl,bc                   ; HL=new position
        ld      (iy+sp_cursorx),l
        ld      (iy+sp_cursorx+1),h     ; update it
        ld      e,(iy+sp_limitx)
        ld      d,(iy+sp_limitx+1)
        inc     de
        ex      de,hl
        and     a
        sbc     hl,bc                   ; HL=max position of char
        and     a
        sbc     hl,de
        call    c,grph_newline          ; move to new line if not in limits
        jp      grph_char_done          ; finished


; ======================================================================
; Internal data
; ======================================================================

.data_glyphbuf
        defs    8               ; glyph buffer, 8 bytes
.data_fixedwidths
        defs    MAXFONT+1       ; fixed font widths
.fillropbuf
        defs    256             ; ROP fill buffer, 256 bytes
.floodbuf
        defs    256             ; floodfill buffer, 256 bytes
