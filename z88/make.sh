# ****************************************************************************
# CamelForth for the Zilog Z80
# Copyright (c) 1994,1995 Bradford J. Rodriguez
# With contributions by Douglas Beattie Jr., 1998
# Widely extended and reorganised by Garry Lancaster, 1999-2011
# Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ****************************************************************************

#
# Build Z88 CamelForth
#

# This script generates an application bank (camel.63) which can be written
# to an EPROM card using ROMCombiner, or a similar utility.
# A glossary (glossary.txt) is also automatically generated.

cp ../generic/* .
z80asm -b -nl -ns -nm -DBUILD_PRJFILE -ocamel.prj options.asm
gforth preprocess.fth
z80asm -b -l -ns -nm -ocamel.63 camel.asm

# Generate glossary
grep ';[[:upper:]]' camel.lst | cut --delimiter=";" -f2- | cut -c3- | LC_ALL=C sort > glossary.txt
