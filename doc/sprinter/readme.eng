Sprinter CamelForth v4.01 - Garry Lancaster, 2002
=================================================

Sprinter CamelForth is a full-featured modern ANS-compliant Forth, which
was developed from an original minimal implementation by Bradford Rodriguez,
and improved and extended by Garry Lancaster. It has been running on the
Z88 since 1999 and ported to the Sprinter in 2002.

As CamelForth is based on the 1994 ANS standard, most of the words can be
found in that documentation. This is available at:

  http://www.zxplus3e.plus.com/z88forever/forth/ansforth/dpans.htm

Some documentation on some of the additional features provided with
Sprinter CamelForth is also available in various *.txt files. Included
is the file ANSDOC.TXT which provides the required additional system-specific
ANS documentation. There are also some utilities and example files in the
various *.FTH files.

To start CamelForth, simply type CAMEL to start the program. You can also
supply some Forth commands on the command line, and they will be executed
immediately. For example,  CAMEL WORDS  will start CamelForth and list all
the words in the dictionary.

To see a demo of the multitasking facilities in CamelForth, run the
DEMO.BAT file.

There are also some loadable Forth files containing utilities and demos.
These can be loaded with:

  S" FILENAME.FTH" INCLUDED


