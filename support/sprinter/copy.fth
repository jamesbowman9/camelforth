\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading COPY command...)
\ An example of using CamelForth to create EXE commands


\ First some useful string-handling words

: -LEADING ( c u -- c' u' )
   BEGIN  OVER C@ BL = OVER 0<> AND  WHILE  1 /STRING  REPEAT ;

: S+ ( c1 u1 c2 u2 -- PAD u3 )
   2SWAP DUP >R PAD SWAP CMOVE TUCK PAD R@ + SWAP CMOVE R> + PAD SWAP ;


\ DSS (v1.55) gets *very* confused if you try and work with files open
\ on more than one disk or directory at once. Additionally, opening a
\ file with a pathname (eg "\test\test.txt") will cause the current
\ directory to be changed to the one containing the file...
\ Because of this we need to keep track of the disk and directory for
\ the source and destination, and make sure we set them correctly
\ before every file access word. We also need to remember the original
\ directory and disk, so we can reset this at the end.

CREATE cmddir 256 ALLOT
CREATE srcdir 256 ALLOT
CREATE dstdir 256 ALLOT
VARIABLE cmddsk
VARIABLE srcdsk
VARIABLE dstdsk

: setcmd ( -- )  cmddsk @ CHDISK THROW cmddir CHDIR THROW ;
: setsrc ( -- )  srcdsk @ CHDISK THROW srcdir CHDIR THROW ;
: setdst ( -- )  dstdsk @ CHDISK THROW dstdir CHDIR THROW ;
: getcmd ( -- )  CURDISK THROW cmddsk ! cmddir CURDIR THROW ;
: getsrc ( -- )  CURDISK THROW srcdsk ! srcdir CURDIR THROW ;
: getdst ( -- )  CURDISK THROW dstdsk ! dstdir CURDIR THROW ;


\ Some words to test if the filename contains a drive letter, or ends
\ with a backslash (\)

: dir? ( c u -- c u f )
   2DUP 1- CHARS + C@ [CHAR] \ = OVER 0<> AND ;

: drive? ( c u -- c u f )
   OVER CHAR+ C@ [CHAR] : = OVER 1 > AND ;


\ Change any relative path into an absolute path for convenience

: setpath ( c u -- c' u' )
   drive? 0= >R 2DUP [CHAR] \ SCAN NIP R> AND
   IF  OVER C@ [CHAR] \ <>
       IF  cmddir 0>S S" \" S+ 2SWAP S+  THEN
   THEN ;


\ Copy the file contents using a 1K buffer

CREATE copybuff 1024 ALLOT

: copycontents ( srcid dstid -- )
   >R  BEGIN  setsrc DUP copybuff 1024 ROT READ-FILE THROW ?DUP
       WHILE  setdst copybuff SWAP R@ WRITE-FILE THROW
       REPEAT  R> 2DROP ;


\ Words to validate the source and destination

: letter? ( c1 C2 -- f )
   2DUP = >R 32 OR = R> OR ;

: overwrite? ( -- )
   ." File exists - overwrite (Y/N)?" CR
   BEGIN  KEY DUP [CHAR] N letter? ABORT" Aborted" [CHAR] Y letter?  UNTIL ;

CREATE newdest 256 ALLOT

: formdest ( c1 u1 c2 u2 -- c1 u1 c2' u2' )
   2OVER BEGIN  2DUP [CHAR] \ SCAN DUP  WHILE  2SWAP 2DROP 1 /STRING  REPEAT
   2DROP S+ TUCK newdest SWAP CMOVE newdest SWAP ;

: checkdest ( c1 u1 c2 u2 -- c1 u1 c2' u2' )
   setcmd 2DUP FILE-STATUS NIP
   IF   dir? IF  formdest  THEN
   ELSE  overwrite?  THEN ;

: checksrc ( c u -- )
   setcmd FILE-STATUS NIP ABORT" File not found" ;


\ Parsing words

: getparam ( c1 u1 -- c2 u2 c1' u1' )
   -LEADING 2DUP BL SCAN ROT OVER - ROT ROT ;

: copyhelp
   ." Usage:  COPY sourcefile dest" CR
   ."         'dest' may be a file or directory (end with \)" CR ;

: .error ( 4*x n | 0 -- )
   ?DUP 0= IF  EXIT  THEN
   -2 = IF  ABORT"S 2@ TYPE CR
        ELSE  ." File access error" CR
        THEN  2DROP 2DROP ;

: .fname ( c u -- c u )
   drive? 0=
   IF  CURDISK THROW [CHAR] A + EMIT [CHAR] : EMIT  THEN
   2DUP TYPE ;


\ The command itself

: docopy ( c1 u1 c2 u2 -- )
   2OVER checksrc checkdest 2SWAP
   ." Copying " setpath setcmd .fname R/O OPEN-FILE THROW getsrc >R
   ."  to " setpath setcmd .fname CR W/O CREATE-FILE THROW getdst R>
   SWAP 2DUP copycontents
   setdst CLOSE-FILE THROW setsrc CLOSE-FILE THROW ;

: copycmd
   getcmd
   CMDLINE COUNT -TRAILING getparam getparam NIP OVER 0= OR
   IF  copyhelp  ELSE  ['] docopy CATCH .error  THEN
   setcmd BYE ;


\ Make our top-level word the first thing Forth executes
' copycmd IS (COLD)

\ Create the EXE file
S" copy.exe" SAVE-EXE

CR .( COPY.EXE has been created)

