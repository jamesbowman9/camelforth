\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

\ Example application for Sprinter CamelForth

\ First load the ANS Forth standard program
S" pyramid.fth" INCLUDED

\ Add a top-level definition
: PYRAMID
    CINIT                            \ Initialise the console window
    ['] GAME CATCH                   \ Execute the program, catching errors
    ?DUP
    IF  CR ." Exception " . ." has occurred." \ Display unexpected errors
        CR ." Please report to programmer. Press a key to exit."
        KEY DROP
    THEN
    BYE ;                            \ Close down the application

\ Set the application entry from COLD
' PYRAMID IS (COLD)

\ Save the EXE file
S" pyramid.exe" SAVE-EXE

CR .( PYRAMID.EXE created)
