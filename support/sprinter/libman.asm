; L0 LIBRARY MANAGER
; for SPRINTER v1.1 
; v0.0 (27.06.2002)
; v1.0 (22.07.2002)
; v1.1 (23.07.2002)
; Alexander Shabarshin (shaos@mail.ru)
; http://www.shaos.ru

        module libman
        org $4000

; CODE MUST BE FROM $4000 TO $BFFF !!!

.L_LOAD jp      _L_LOAD
.L_FREE jp      _L_FREE
.L_CALL jp      _L_CALL
.L_INFO jp      _L_INFO

; <><><><><><><><><><><><><><><><><>
; Data about loaded libraries
; 0: 00-empty, XX-loaded
; 1: memory block identifier
; 2: start hi-address
; 3: stop hi-address
; 256 bytes for 64 loaded libraries

defc    LIBRM = 64

.LIBR    defs      256


; <><><><><><><><><><><><><><><><><>
;       ld      hl,filename
;       ld      a,win ; 1,2,3
;       call    l_load
;       jp      c,error
;       ld      (handle),hl

._L_LOAD
	push	ix
	push	iy
	push	de
        push    af              ; 1
        push    hl              ; 2
        jp      c,llerr1
        in      a,($E2)
        ld      (lloldw),a
        ld      bc,$023D        ; GETMEM (2)
        rst     $10
        ld      (llid),a
        ld      bc,$003B        ; SETWIN3 (0)
        rst     $10
        jp      c,llerr1
        pop     hl              ; 1
        ld      c,$11           ; OPEN
        ld      a,1
        rst     $10
        jp      c,llerr2
        ld      (llhand),a
        ld      bc,$0215        ; MOVE_FP
        ld      hl,0
        ld      ix,0
        rst     $10
        jp      c,llerr0
        ld      a,h
        or      l
        jp      nz,llerr0
        push    ix              ; 2
        ld      a,(llhand)
        ld      bc,$0015        ; MOVE_FP
        ld      hl,0
        ld      ix,0
        rst     $10
        pop     hl              ; 1
        ld      (llsize),hl
        jp      c,llerr0
        ld      c,$13           ; READ
        ex      de,hl
        ld      hl,$C000
        ld      a,(llhand)
        rst     $10
        jp      c,llerr0
        ld      a,(llhand)
        ld      c,$12           ; CLOSE
        rst     $10
        jp      c,llerr2
        ld      hl,$C000
        ld      d,h
        ld      e,l
        ; hl - enpacked data in 0 page
        ; de - depacking data in 1 page
.ll1    ld      bc,16   ; buff size
        push    de              ; 2 d
        ld      de,llbuf
        di
        ld      d,d
        ld      a,16    ; buff size
        ld      b,b
        ld      l,l
        ld      a,(hl)
        ld      (de),a
        ld      b,b
        ei
        add     hl,bc
        push    hl              ; 3 e+ d
        push    bc              ; 4 b e+ d
        ld      a,(llid)
        ld      bc,$013B        ; SETWIN3 (1)
        rst     $10
        pop     bc              ; 3 e+ d
        pop     hl              ; 2 d
        pop     de              ; 1
        jp      c,llerr2
        push    hl              ; 2 e+
        ld      hl,llbuf
        ld      a,d
        cp      $C0
        jr      nz,ll2
        ld      a,e
        cp      $10
        jr      nc,ll2
        di
        ld      d,d
        ld      a,16    ; buff size
        ld      b,b
        ld      l,l
        ld      a,(hl)
        ld      (de),a
        ld      b,b
        ei
        ex      de,hl
        add     hl,bc   ; de+ for simple copy
        ex      de,hl
        jp      ll3
.ll2    ld      a,(llzero)
        or      a
        jr      z,ll2a
        xor     a
        ld      (llzero),a
        jr      ll2c
.ll2a   ld      a,(hl)
        or      a
        jr      nz,ll2b
        inc     hl
        dec     c
        jr      z,ll2e
.ll2c   ld      (de),a
        inc     de      ; de++ for decoding
        dec     (hl)
        jr      nz,ll2c
        inc     hl
        jr      ll2d
.ll2e   ld      a,1
        ld      (llzero),a
        jr      ll3
.ll2b   ld      (de),a
        inc     hl
        inc     de
.ll2d   dec     c
        jr      nz,ll2a
.ll3    push    de              ; 3 d+ e+
        ld      a,(llid)
        ld      bc,$003B        ; SETWIN3 (0)
        rst     $10
        pop     de              ; 2 e+
        pop     hl              ; 1
        ld      a,(llsize+1)
        ld      b,a
        ld      a,h
        sub     $C0
        cp      b
        jp      c,ll1
        jp      nz,ll4
        ld      a,(llsize)
        ld      b,a
        ld      a,l
        cp      b
        jp      c,ll1
.ll4    xor     a
        ld      (llzero),a
        ld      a,(llid)
        ld      bc,$013B        ; SETWIN3 (1)
        rst     $10
        ld      hl,$C004
        ld      e,(hl)
        inc     hl
        ld      d,(hl)
        inc     hl
        ex      de,hl
        push    hl
        pop     iy              ; for REMAKE
        dec     hl              ; ???
        ld      (llsize),hl
        ld      a,(llid)
        ld      bc,$003B        ; SETWIN3 (0)
        rst     $10
        ld      hl,$C000
        di
        ld      d,d
        ld      a,0
        ld      b,b
        ld      c,c
        ld      (hl),a
        ld      b,b
        ei
        ld      hl,LIBR
        ld      c,LIBRM
.ll5    ld      a,(hl)
        or      a
        jr      z,ll5c
        push    hl
        pop     ix
        ld      h,$C0
        ld      l,(ix+1)
        ld      a,(ix+3)
        and     $3F
        inc     a
        cp      (hl)
        jr      c,ll5a
        ld      (hl),a
.ll5a   push    ix
        pop     hl
.ll5c   inc     hl
        inc     hl
        inc     hl
        inc     hl
        dec     c
        jr      nz,ll5
        ld      hl,LIBR
        ld      c,LIBRM
.ll5b   ld      a,(hl)
        or      a
        jr      z,ll6
        inc     hl
        inc     hl
        inc     hl
        inc     hl
        dec     c
        jr      nz,ll5b
        jp      llerr2
.ll6    push    hl
        pop     ix
        ld      hl,$C000
        ld      bc,$0100
.ll7    ld      a,(hl)
        or      a
        jr      nz,ll8
.ll7a   inc     hl
        dec     bc
        ld      a,b
        or      c
        jr      nz,ll7
        ld      bc,$013D        ; GETMEM (1)
        rst     $10
        jp      c,llerr3
        ld      l,a
        ld      d,0
        ld      a,(llsize+1)
        jr      ll8c
.ll8    ld      d,a
        ld      a,(llsize+1)
        add     a,d
        cp      $40
        jr      nc,ll7a
.ll8c   ld      e,a
        pop     af              ; 0
        ; A - win (1,2,3)
        cp      1
        jr      nz,ll8a
        ld      a,$40
        jr      ll9
.ll8a   cp      2
        jr      nz,ll8b
        ld      a,$80
        jr      ll9
.ll8b   ld      a,$C0
.ll9    push    af
        add     a,d
        ld      d,a
        pop     af
        add     a,e
        ld      e,a
        ld      (ix+0),$FF
        ld      (ix+1),l
        ld      (ix+2),d
        ld      (ix+3),e
	push	de
        ld      a,(llid)
        ld      bc,$013B        ; SETWIN3 (1)
        rst     $10
	pop	de
        ld      hl,(llsize)
        ld      bc,$C000
        ; iy already done
        add     iy,bc
        ld      b,h
        ld      c,l
        ld      e,0
        ld      hl,$C000
        push    ix
        call    REMAKE
        pop     ix
        ld      hl,$C000
        ld      a,(ix+2)
        or      $C0
        ld      d,a
        ld      e,0
.ll10   push    de
        ld      de,llbuf
        di
        ld      d,d
        ld      a,16    ; buff size
        ld      b,b
        ld      l,l
        ld      a,(hl)
        ld      (de),a
        ld      b,b
        ei
        pop     de
        push    hl
	push	de
        ld      a,(ix+1)
        ld      bc,$003B        ; SETWIN3 (?)
        rst     $10
	pop	de
        ld      hl,llbuf
        di
        ld      d,d
        ld      a,16    ; buff size
        ld      b,b
        ld      l,l
        ld      a,(hl)
        ld      (de),a
        ld      b,b
        ei
	push	de
        ld      a,(llid)
        ld      bc,$013B        ; SETWIN3 (1)
        rst     $10
	pop	de
        pop     hl
        ld      bc,16   ; buff size
        add     hl,bc
        ex      de,hl
        add     hl,bc
        ex      de,hl
        ld      a,(ix+3)
        or      $C0
        cp      d
        jp      nc,ll10

        ld      a,(ix+1)
        ld      bc,$003B        ; SETWIN3 (?)
        rst     $10
        push    ix
        ld      a,(llid)
        ld      c,$3E           ; FREEMEM
        rst     $10
        jp      c,llerr3
        pop     hl
        ld      bc,LIBR
        xor     a
        sbc     hl,bc
        ld      a,l
        rra
        rra
        ld      l,a
        ld      h,0
        ld      a,(lloldw)
        out     ($E2),a
        ld      b,0
        call    L_CALL
	pop	de
	pop	iy
	pop	ix
        xor     a
        ret
.llerr0 ld      a,(llhand)
        ld      c,$12           ; CLOSE
        rst     $10
        jr      llerr2
.llerr1 pop     hl
.llerr2 pop     af
	pop	de
	pop	iy
	pop	ix
.llerr3 scf
        ret

.llsize  defw      0
.llid    defb      0
.llhand  defb      0
.llbuf   defs      16
.llzero  defb      0
.lloldw  defb      0

; bc = code len
; de = new start address
; hl = code address
; iy = reloc table (BitMap)

.REMAKE ld      ix,RELOD1
.RELOC0 ld      (ix+0),8        ; size of byte
        ld      a,(iy+0)
.RELOCA rla
        push    af
        call    c,RELOC1        ; correcting address
        inc     hl
        dec     bc
        ld      a,b
        or      c
        jr      z,RERET
        pop     af
        dec     (ix+0)
        jr      nz,RELOCA
        inc     iy
        jr      RELOC0
.RELOC1 ld      a,(hl)
        add     a,d
        ld      (hl),a
        ret
.RERET  pop     af
        ret
.RELOD1 defb      0               ; work cell


; <><><><><><><><><><><><><><><><><>
;       ld      hl,(handle)
;       call    l_free
;       jp      c,error

._L_FREE
	push	de
	push	bc
        ld      b,1
        call    L_CALL
        ld      d,0
        ld      e,l
        ld      hl,LIBR
        add     hl,de
        add     hl,de
        add     hl,de
        add     hl,de
        ld      a,(hl)
        or      a
        jr      z,lf_er
        ld      (hl),0
        inc     hl
        ld      b,(hl)
        ld      hl,LIBR
        ld      c,LIBRM
        ld      e,0
.lf1    ld      a,(hl)
        jr      z,lf2
        inc     hl
        ld      a,(hl)
        cp      b
        jr      nz,lf3
        inc     e
        jr      lf3
.lf2    inc     hl
.lf3    inc     hl
        inc     hl
        inc     hl
        dec     c
        jr      nz,lf1
        ld      a,e
        or      a
        jr      nz,lf_e
        ld      a,b
        ld      c,$3E           ; FREEMEM
        rst     $10
        jr	lf_ee
.lf_e   xor     a
        jr	lf_ee
.lf_er  scf
.lf_ee  pop     bc
	pop	de
        ret


; <><><><><><><><><><><><><><><><><>
; user data in a,de,ix,iy
;       ld      hl,(handle)
;       ld      b,function
;       call    l_call
;       jp      c,error

._L_CALL
        push    hl
        push    de
        push    bc
        push    af
        ld      a,l
        rla
        rla
        ld      d,0
        ld      e,a
        ld      hl,LIBR
        add     hl,de
        ld      a,(hl)
        or      a
        jr      z,lc_er2
        inc     hl
        ld      a,(hl)
        push    af
        inc     hl
        ld      h,(hl)
        ld      l,$20
        ld      (lcstart),hl
        ld      a,h
        and     $C0
        ld      (lcoldp),a
        cp      $40
        jr      nz,lc1
        in      a,($A2)
        jr      lc3
.lc1    cp      $80
        jr      nz,lc2
        in      a,($C2)
        jr      lc3
.lc2    cp      $C0
        jr      nz,lc_er3
        in      a,($E2)
.lc3    ld      (lcoldw),a
        pop     af
	push	de
        ld      bc,$0038        ; SETWIN
        rst     $10
	pop	de
        pop     af
        pop     bc
        ld      hl,(lcstart)
        ld      d,0
        ld      e,b
        add     hl,de
        add     hl,de
        add     hl,de
        pop     de
        ld      bc,lc_
        push    bc
        jp      (hl)            ; CALL FUNC
.lc_    pop     hl              ; restore handle
	ld	c,a		; save A to C
        ld      a,(lcoldw)
        ld      b,a
        ld      a,(lcoldp)
        cp      $40
        jr      nz,lc1_
        ld      a,b
        out     ($A2),a
        jr      lc3_
.lc1_   cp      $80
        jr      nz,lc2_
        ld      a,b
        out     ($C2),a
        jr      lc3_
.lc2_   cp      $C0
        jr      nz,lc_er0
        ld      a,b
        out     ($E2),a
.lc3_   xor     a               ; reset flag c
	ld	a,c		; restore A
        ret
.lc_er3 pop     af
.lc_er2 pop     af
        pop     bc
        pop     de
.lc_er1 pop     hl
.lc_er0 scf
        ret

.lcstart defw      0
.lcoldp  defb      0
.lcoldw  defb      0

; <><><><><><><><><><><><><><><><><>
; copy lib info
;       ld      hl,(handle)
;       ld      de,buffer32
;       call    l_info
;       jp      c,error

._L_INFO
        push    hl
        push    de
        push    bc
        ld      a,l
        rla
        rla
        ld      b,0
        ld      c,a
        ld      hl,LIBR
        add     hl,bc
        ld      a,(hl)
        or      a
        jr      z,li_er
        inc     hl
        ld      b,(hl)
        inc     hl
        ld      a,(hl)
        or      $C0
        ld      h,a
        ld      l,0
        in      a,($E2)
        ld      (lioldw),a
	push	de
        ld      a,b
        ld      bc,$003B        ; SETWIN3 (0)
        rst     $10
	pop	de
        ld      bc,32
        ldir
        ld      a,(lcoldw)
        out     ($E2),a
        xor     a
        jr      li_ok
.li_er  scf
.li_ok  pop     bc
        pop     de
        pop     hl
        ret

.lioldw  defb      0


; <><><><><><><><><><><><><><><><><>

