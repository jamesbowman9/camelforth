\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading BOOTDSK code word example...)
\ An example of creating a code word accessing DSS/BIOS calls

\ There is currently no assembler, so you must hand-assemble the
\ code for now...

HEX
CODE BOOTDSK ( -- n ior )       \ Begin with CODE (or FCODE, see FASTCODE.TXT)
  C5 C,         \ push bc       \ Push BC (current TOS) on stack
  FD C, E5 C,   \ push iy       \ must preserve IY, IX, DE
  DD C, E5 C,   \ push ix
  D5 C,         \ push de
  06 C, 00 C,   \ ld b,0        \ set B=0 for BOOTDSK
  0E C, 09 C,   \ ld c,$09      \ BOOTDSK call number
  D7 C,         \ rst $10       \ execute DSS call
  D1 C,         \ pop de        \ restore DE, IX, IY
  DD C, E1 C,   \ pop ix
  FD C, E1 C,   \ pop iy
  6F C,         \ ld l,a        \ HL=n, bootdisk number
  26 C, 00 C,   \ ld h,0
  E5 C,         \ push hl       \ place n at 2OS position
  01 C, 0000 ,  \ ld bc,0       \ BC (TOS) = 0 for no error
  30 C, 05 C,   \ jr nc,X       \ move on if no error
  ED C, 44 C,   \ neg
  4F C,         \ ld c,a
  06 C, FE C,   \ ld b,$fe      \ BC (TOS) = -256-DSSerr
                \ X:
NEXT                            \ end with NEXT (or FNEXT, see FASTCODE.TXT)
DECIMAL

