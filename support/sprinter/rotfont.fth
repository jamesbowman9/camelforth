\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading 8x8 font rotation code...)
\ Code to convert standard 8x8 Spectrum fonts into
\ vertically-defined fonts used by CamelForth

HEX
CODE ROTGLYPH ( src dest -- )   \ Rotate a single glyph
  E1 C,         \ pop hl        \ HL=src
  C5 C,         \ push bc
  DD C, E3 C,   \ ex (sp),ix    \ IX=dest
  06 C, 08 C,   \ ld b,8        \ 8 rows
  7E C,         \ loop:ld a,(hl)        \ get next row
  23 C,         \ inc hl
  17 C,                 \ rla
  CBDD , 00 C, 16 C,    \ rl (ix+0)
  17 C,                 \ rla
  CBDD , 01 C, 16 C,    \ rl (ix+1)
  17 C,                 \ rla
  CBDD , 02 C, 16 C,    \ rl (ix+2)
  17 C,                 \ rla
  CBDD , 03 C, 16 C,    \ rl (ix+3)
  17 C,                 \ rla
  CBDD , 04 C, 16 C,    \ rl (ix+4)
  17 C,                 \ rla
  CBDD , 05 C, 16 C,    \ rl (ix+5)
  17 C,                 \ rla
  CBDD , 06 C, 16 C,    \ rl (ix+6)
  17 C,                 \ rla
  CBDD , 07 C, 16 C,    \ rl (ix+7)
  10 C, D4 C,   \ djnz loop
  DD C, E1 C,   \ pop ix
  C1 C,         \ pop bc
NEXT
DECIMAL

: ROTFONT ( src dest n -- )     \ Rotate font consisting of n glyphs
   FOR  2DUP ROTGLYPH 8 CHARS + SWAP 8 CHARS + SWAP  STEP  2DROP ;


