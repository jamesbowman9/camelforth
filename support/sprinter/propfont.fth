\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading proportional font conversion code...)
\ Code to convert Anton Enin's 640-mode proportional fonts (see aprint
\ example) into fonts used by CamelForth
\ Doesn't clip wide glyphs (they may be usable in 640 mode) but does
\ show warning

HEX
CODE CNVGLYPH ( src dest bytewidth -- )   \ Convert a single glyph
  E1 C,         \ pop hl        \ HL=dest
  DD C, E3 C,   \ ex (sp),ix    \ IX=src
  D5 C,         \ push de
                \ .lineloop
  06 C, 08 C,   \ ld b,8        \ 8 rows
                \ .rowloop
  DD C, 7E C, 00 C, \ ld a,(ix+0)
  DD C, 23 C,   \ inc ix
  17 C,         \ rla           \ Fc=left column
  CB C, 12 C,   \ rl d          \ rotate into D
  1F C,         \ rra
  1F C,         \ rra           \ Fc=right column
  CB C, 13 C,   \ rl e          \ rotate into E
  10 C, F2 C,   \ djnz rowloop
  72 C,         \ ld (hl),d
  23 C,         \ inc hl
  73 C,         \ ld (hl),e
  23 C,         \ inc hl
  0D C,         \ dec c
  20 C, E9 C,   \ jr nz,lineloop
  D1 C,         \ pop de
  DD C, E1 C,   \ pop ix
  C1 C,         \ pop bc
NEXT
DECIMAL

: CNVFONT ( src dest -- )       \ Convert font
   DUP >R
   256 0 DO  OVER I + C@ 2*
             DUP 8 > IF  CR ." WARNING: Wide glyph " I . DUP .  THEN
             OVER I + C!
         LOOP
   256 +
   256 0 DO  OVER DUP I + >R
             R@ 256 + C@  R@ 512 + C@  256 * + +
             OVER R@ C@ CNVGLYPH
             R> C@ 2* +
         LOOP
   NIP CR ." Final font size in bytes: " R> - . ;
