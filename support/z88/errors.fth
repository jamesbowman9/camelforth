\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2014
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2014
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading error reporting...)

HEX

CODE GN_ESP ( rc -- addr x )
  79 C,              \ ld a,c
  E7 C, 4C09 ,       \ call_oz(gn_esp)
  E5 C,              \ push hl
NEXT

CODE GN_RBE ( addr x -- addr' x c )
  E1 C,              \ pop hl  (BHL=extended address)
  E7 C, 3E09 ,       \ call_oz(gn_rbe)
  23 C,              \ inc hl
  E5 C,              \ push hl
  C5 C,              \ push bc
  4F C,              \ ld c,a
  06 C, 00 C,        \ ld b,0  (TOS=char)
NEXT

DECIMAL

: .OZERR ( rc -- )
   GN_ESP BEGIN  GN_RBE ?DUP  WHILE  EMIT  REPEAT  2DROP ;

: .ANYERROR ( rc -- )
     CASE -35 OF  ." Invalid block"  ENDOF
          -49 OF  ." Search-order overflow"  ENDOF
          -4095 OF  ." Non-fastable word: " ABORT"S 2@ TYPE  ENDOF
          -4094 OF  ." Input nesting error"  ENDOF
          -4093 OF  ." Region error"  ENDOF
          -4092 OF  ." Blocks already allocated"  ENDOF

          DUP NEGATE 256 /MOD  ( rc -rclo -rchi )
          CASE  1 OF  .OZERR  ENDOF
                2 OF  ." Package "
                      BASE @ HEX SWAP . BASE !
                      ." not found"
                  ENDOF
                
                NIP OVER (.ERR)
          ENDCASE
     ENDCASE ;

' .ANYERROR IS .ERROR

